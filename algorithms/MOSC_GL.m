function [err_node,NMI,err_triangle,err_edge,cut_order_array,t_Lap_eigen_cut,G,part_G,P_no_seloop,total_motifs,true_labels,n_node,isrealData,iterated_cluster_num,targeted_cluster,actual_labels_MinMax,num_communities,seperated_label] = MOSC2(G,part_G,P_no_seloop,total_motifs,true_labels,n_node,isrealData,iterated_cluster_num,targeted_cluster,actual_labels_MinMax,num_communities,seperated_label,isrefined,lambda,dataset,top_num,row_combins,cut_order_array,motif_adj,bi_or_multiple,total_edges,total_triangles,true_label_index,num_clusters)
tic

W_mix = (1-lambda)*motif_adj + lambda * G;

% aggressive for edge structure due to 2 times?

% starting to construct mixed-order degree matrix

D_mix = sparse(1:size(G, 1), 1:size(G, 2), sum(W_mix,2));

% mixed-order Laplacian
L_mix = D_mix - W_mix;
% avoid dividing by zero
d_mix = diag(D_mix);
d_mix(d_mix ~= 0) = 1 ./ sqrt(d_mix(d_mix ~= 0));
% calculate D^(-1/2)
D_mix = spdiags(d_mix, 0, size(W_mix, 1), size(W_mix, 2));
% calculate normalized Laplacian
L_mix = D_mix * L_mix * D_mix;
% disp('MOSC2')
% disp(issymmetric(L_mix))
t_Lap = toc;

algType = 'MOSC2';

if strcmp(bi_or_multiple,'bi')
    tic
    [eigvec,last_two_eigvec] = sort_eigen(L_mix,algType,n_node);
    t_cal_eigen = toc;
    % sort nodes with the normalized mixed-order degree
    [~, cut_order] = sort(eigvec./sqrt(d_mix),'descend');
    isrefined = 0;

    % save(['result/interm/cut_order/' dataset '/Science/top_' int2str(top_num) '/' int2str(row_combins) '.mat'],'cut_order','G','P_no_seloop','n_node','cut_order','total_motifs','true_label','isrefined','isrealData')
    
    [~,err_node,NMI,err_triangle,err_edge,~,~,t_sweep_cut] = cut_strategies(G,P_no_seloop,n_node,cut_order,total_motifs,true_labels,isrefined,isrealData,last_two_eigvec,algType,motif_adj,lambda);
    t_Lap_eigen_cut = [t_Lap;t_cal_eigen;t_sweep_cut];
    
    if isrealData == 1
        [n_node,targeted_cluster,seperated_label,iterated_cluster_num,actual_labels_MinMax,part_G,total_motifs] = multiple_partitions(G,P_no_seloop,n_node,cut_order,total_motifs,true_labels,isrefined,isrealData,iterated_cluster_num,seperated_label,targeted_cluster,actual_labels_MinMax,isTSC);
    end
else
    [err_node,NMI,err_edge,err_triangle,~,cut_order_array,t_Lap_eigen_cut] = multi_partition_After_L(L_mix,algType,n_node,num_clusters,true_labels,true_label_index,G,total_edges,total_triangles,dataset,P_no_seloop);
    t_Lap_eigen_cut = [t_Lap;t_Lap_eigen_cut];
end


err_node = best_cut_criteria_result(err_node,n_node);
NMI = best_cut_criteria_result_NMI(NMI);
err_edge = best_cut_criteria_result(err_edge,nnz(G)/2);
err_triangle = best_cut_criteria_result(err_triangle,total_motifs/3);
end

function result = best_cut_criteria_result(result,num)
% delete optimal cut since it uses ground-truth
result(6) = [];
% minimum one is the best one
result = min(result)/num;
end

function result = best_cut_criteria_result_NMI(result)
% delete optimal cut since it uses ground-truth
result(6) = [];
% maximum one is the best one
result = max(result);
end
