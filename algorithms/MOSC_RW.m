function [err_node,NMI,err_triangle,err_edge,cut_order_array,t_Lap_eigen_cut,G,part_G,P_no_seloop,total_motifs,true_labels,n_node,isrealData,iterated_cluster_num,targeted_cluster,actual_labels_MinMax,num_communities,seperated_label] = MOSC_RW(RW_matrix,G,part_G,P_no_seloop,total_motifs,true_labels,n_node,isrealData,iterated_cluster_num,targeted_cluster,actual_labels_MinMax,num_communities,seperated_label,isrefined,lambda,dataset,top_num,row_combins,cut_order_array,tansition_tensor,motif_adj,bi_or_multiple,total_edges,total_triangles,true_label_index,num_clusters)
isTSC = 1;
tic
[W_sum_dan_tensor] = sum_dangling_tensor(tansition_tensor,n_node);

hybrid_matrix = (1-lambda)*W_sum_dan_tensor + lambda*RW_matrix;
t_Lap = toc;

algType = 'Hybrid';

if strcmp(bi_or_multiple,'bi')
    tic
    [eigvec,last_two_eigvec] = sort_eigen(hybrid_matrix,algType,n_node);
    
    t_cal_eigen = toc;
    [~, cut_order] = sort(eigvec,'descend'); % sort nodes
    isrefined = '0';
    
    % [cut_order_array] = save_cut_order(cut_order_array,cut_order,lambda,row_combins);
%     cut_order_array{coloum,row_combins} = cut_order';
    
    [~,err_node,NMI,err_triangle,err_edge,~,~,t_sweep_cut] = cut_strategies(G,P_no_seloop,n_node,cut_order,total_motifs,true_labels,isrefined,isrealData,last_two_eigvec,algType,motif_adj,lambda);
    t_Lap_eigen_cut = [t_Lap;t_cal_eigen;t_sweep_cut];
    if isrealData == 1
        [n_node,targeted_cluster,seperated_label,iterated_cluster_num,actual_labels_MinMax,part_G,total_motifs] = multiple_partitions(G,P_no_seloop,n_node,cut_order,total_motifs,true_labels,isrefined,isrealData,iterated_cluster_num,seperated_label,targeted_cluster,actual_labels_MinMax,isTSC);
    end
else
    [err_node,NMI,err_edge,err_triangle,~,cut_order_array,t_Lap_eigen_cut] = multi_partition_After_L(hybrid_matrix,algType,n_node,num_clusters,true_labels,true_label_index,G,total_edges,total_triangles,dataset,P_no_seloop);
    t_Lap_eigen_cut = [t_Lap;t_Lap_eigen_cut];
end

err_node = best_cut_criteria_result(err_node,n_node);
NMI = best_cut_criteria_result_NMI(NMI);
err_edge = best_cut_criteria_result(err_edge,nnz(G)/2);
err_triangle = best_cut_criteria_result(err_triangle,nnz(tansition_tensor)/6);
end

function result = best_cut_criteria_result(result,num)
% delete optimal cut since it uses ground-truth
result(6) = [];
% minimum one is the best one
result = min(result)/num;
end

function result = best_cut_criteria_result_NMI(result)
% delete optimal cut since it uses ground-truth
result(6) = [];
% maximum one is the best one
result = max(result);
end

