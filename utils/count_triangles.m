% load('data/synthetic/2D_adj_matrix/multi_partition_G.mat');
% num_triangle = count_triangles1(multi_partition_G);

function num_triangle = count_triangles(cluster)
node_num = size(cluster,2);
all_coms = nchoosek(1:node_num,3);
row_num = size(all_coms,1);
num_triangle = 0;
for row = 1:row_num
    if cluster(all_coms(row,1),all_coms(row,2)) == 1 && cluster(all_coms(row,1),all_coms(row,3)) == 1 && cluster(all_coms(row,2),all_coms(row,3)) == 1
        num_triangle = num_triangle + 1;
    end
end
% disp(num_triangle)
% iterating all permutations
% tic
% for kk = 1:20
% for i = 1:node_num
%     for j = 1:node_num
%         for k = 1:node_num
%             if cluster(i,j) == 1 && cluster(j,k) == 1 && cluster(k,i) == 1
%                 num_triangle = num_triangle + 1;
%             end
%         end
%     end
% end
% end
% toc
% num_triangle = num_triangle/6;
end

