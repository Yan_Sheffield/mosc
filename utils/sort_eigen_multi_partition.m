function eigvecs = sort_eigen_multi_partition(W,algType,node_num,num_cluster)

% largest eigenvector
if strcmp(algType,'TTM') || strcmp(algType,'TSC') || strcmp(algType,'MinMax')||strcmp(algType,'Hybrid')||strcmp(algType,'RW_SC')||strcmp(algType,'HOSVD')||strcmp(algType,'MOSC_RW_2')
    [eigvecs,~] = eigs(W,num_cluster,'largestabs');
    if strcmp(algType,'TTM') || strcmp(algType,'HOSVD')
        eigvecs = norm_step(eigvecs,node_num);
    end
end

% calculate second smallest eigenvector
if strcmp(algType,'norm_SC') || strcmp(algType,'MOSC2') ||strcmp(algType,'Science')|| strcmp(algType,'MOSC2_norm')
    if  det(W) == 0
        [eigvecs,~] = eigs(W,num_cluster,10^-12);
    else
        [eigvecs,~] = eigs(W,num_cluster,'smallestabs');
    end     
     eigvecs = norm_step(eigvecs,node_num);
end

end


function[eigvec] =  norm_step(eigvec,node_num)
for i = 1:node_num
    if (norm(eigvec(i,:))>0)
        eigvec(i,:) = eigvec(i,:)./norm(eigvec(i,:));
    end
end
end

