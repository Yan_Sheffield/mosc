function [x,V,t_Lap,t_cal_eigen,lambda] = nfiedler(A)
% NFIEDLER returns the fiedler vector of the normalized laplacian of A.

if nargin < 2
    tol = 1e-12;
end

L = nlaplacian(A);
t_Lap = toc;
n = size(A, 1);
tic
[V, lambdas] = eigs(L + speye(n), 2 , 'sa', struct('tol', tol));
t_cal_eigen = toc;
[~, eig_order] = sort(diag(lambdas));
ind = eig_order(end);
x = V(:, ind);
x = x ./ sqrt(sum(A, 2));
lambda = lambdas(ind, ind) - 1;
