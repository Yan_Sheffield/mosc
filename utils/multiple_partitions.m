function[n_node,targeted_cluster,seperated_label,iterated_cluster_num,actual_lables,part_G,total_motifs] = multiple_partitions(G,P_no_seloop,n_node,cut_order,total_motifs,true_label,isrefined,isrealData,iterated_cluster_num,seperated_label,targeted_cluster,actual_lables,isTSC)
[conduc,~,~,~,~,~,~,~,~,~,~,~,~,best_labelled_conduc,~] = sweepCut(G,P_no_seloop,n_node,cut_order,total_motifs,true_label,isrefined,isrealData);
    
    % these should be two new groups
    [c_sub,c_sub_bar] = grouped_nodes(best_labelled_conduc);
    if iterated_cluster_num == 1
        c_one_size = size(c_sub,1);
        c_two_size = size(c_sub_bar,1);
        if c_one_size >= c_two_size
            part_G = G(c_sub,c_sub);
            n_node = c_one_size;
            targeted_cluster = c_sub;% target = [1 2 3]
            seperated_label = best_labelled_conduc(c_sub(1));
        else
            part_G = G(c_sub_bar,c_sub_bar);
            n_node = c_two_size;
            targeted_cluster = c_sub_bar;
            seperated_label = best_labelled_conduc(c_sub_bar(1));
        end
        iterated_cluster_num = iterated_cluster_num + 1;
        actual_lables = best_labelled_conduc;% [1 1 1 2 2 2]
    else
        % merge with all clusters. c_sub is the current position.Elements in target_cluster  
        % are also postion. targeted_cluster(c_sub) corresponds the ture
        % label in whole graph. 
        actual_lables(targeted_cluster(c_sub)) = seperated_label;
        iterated_cluster_num = iterated_cluster_num + 1;
        actual_lables(targeted_cluster(c_sub_bar)) = iterated_cluster_num;
        % element in the largest component
        seperated_label = mode(actual_lables);       
        % the postion of most frenquent element in actual_lables_TSC
        [targeted_cluster,~,~] = find(actual_lables == seperated_label);
        n_node = size(targeted_cluster,1);
%         targeted_cluster = extracted_positions;
        part_G = G(targeted_cluster,targeted_cluster);
    end
     
    [P,P_no_seloop,vec] = transTensor(part_G); % P_no_seloop is for counting cut/vol/assoc
    if isTSC == 1
        [part_G] = add_dangling_vector(part_G,P,vec);% p_dangling for TSC
    end    
    total_motifs = nnz(P_no_seloop)/2;     
end
% TODO: has a better way to do it 
function[cluster_one,cluster_two] = grouped_nodes(cluster_indices)
[cluster_one,~,~] = find(cluster_indices==1);
[cluster_two,~,~] = find(cluster_indices==2);
end
