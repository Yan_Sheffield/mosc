% lambda = [0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,10^10];
% ind = 1:length(lambda);

% youtube error nodes
% EN_20 = [1.4895 1.5947  1.5474  1.5842  1.5684  1.6263  1.6053  1.6053  1.5947 1.6158];
% EN_50 = [2.1494	1.8049	1.8147	1.8686	1.9216	1.9420	1.9682	2.0073	2.0131	2.177142857];
% best_competitor_EN_20 = 1.9;
% best_competitor_EN_50 = 2.41;
% plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50);

% youtube error triangle
% EN_20 = [5.3105	3.8053	3.9737	3.9737	3.9789	3.7789	3.8263	3.7842	3.7842	3.8053];
% best_competitor_EN_20 = 16.0368;
% EN_50 = [7.8612	5.1771	5.0408	4.7045	5.1771	5.2522	5.3282	5.1951	5.4229	7.474285714];
% best_competitor_EN_50 = 10.6122;
% plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50);

% 
% % DBLP error nodes
EN_20 = [0.8737	0.1158	0.1158	0.1158	0.1158	0.1158	0.1158	0.1158	0.1158	0.1158];
best_competitor_EN_20 = 0.1158;
EN_50 = [1.1151	0.5902	0.2849	0.0351	0.0351	0.0351	0.0351	0.0351	0.0351	0.0351 ];
best_competitor_EN_50 = 0.0669;
plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50);
% 
% % DBLP error triangles
% EN_20 = [62.5053	28.1526	16.8000	13.0737	5.5684	10.5947	75.7368	5.0421	11.3316	76.5263];
% best_competitor_EN_20 = 0.2211;
% EN_50 = [59.8351	31.3706	16.1829	5.6082	3.1020	4.3706	16.2498	4.9486	6.4563	24.8049];
% best_competitor_EN_50 = 0.6416;
% plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50);

% % orkut error nodes
% EN_20 = [0.2526	0.2895	0.3158	0.3105	0.3000	0.3000	0.3000	0.3000	0.3053	0.289473684];
% best_competitor_EN_20 = 0.3263;
% EN_50 = [0.9339	0.9592	0.9167	0.9151	0.8947	0.9069	0.9273	0.9078	0.9167	0.9192];
% best_competitor_EN_50 = 0.8033;
% plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50);

% orkut error triangles
% EN_20 = [25.0316	49.8000	49.8000	50.8368	50.8368	45.1421	45.1421	45.1421	45.1421	47.38947368];
% best_competitor_EN_20 = 81.1526;
% EN_50 = [134.0139	164.9861	136.2416	135.2212	137.0465	135.9624	135.9624	135.9624	135.9624	135.6637];
% best_competitor_EN_50 = 187.7004;
% plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50);


% LJ error nodes
% EN_20 = [2.7421	2.7579	2.7263	2.8105	2.7211	2.7737	2.8158	2.7842	2.7789	2.621052632];
% best_competitor_EN_20 = 3.3842;
% EN_50 = [1.8727	1.8392	1.8245	1.8522	1.8465	1.8237	1.8506	1.8514	1.8351	1.8016];
% best_competitor_EN_50 = 1.6996;
% plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50);

% LJ error triangles
% EN_20 = [7025.5105	5098.3211	5026.2316	6437.7684	4778.8684	4778.8684	3530.5263	3530.5263	3530.5211	3708.431579];
% best_competitor_EN_20 = 9683.5263;
% EN_50 = [644.1453	620.5298	579.7951	566.3029	557.4327	556.7445	556.2882	558.0424	558.0416	552.0873];
% best_competitor_EN_50 = 2170.0473;
% plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50);
 
 
function plot_EN_ET(EN_20,EN_50,best_competitor_EN_20,best_competitor_EN_50)
figure;
ind = [0.2,0.4,0.6,0.8,1,1.2,1.4,1.6,1.8,10^10];
ind = 1:length(ind);
fig1 = plot(ind,EN_20,'--o',ind,best_competitor_EN_20*ones(size(EN_20)),'--','Color','red');
hold on
fig2 = plot(ind,EN_50,'-*',ind,best_competitor_EN_50*ones(size(EN_50)),'-','Color','black');
legend({'MSC@20','Best@20','MSC@50','Best@50'},'Position',[0.7 0.6 0.15 0.3],'FontSize',16)
set(fig1,'LineWidth',1.8);
set(fig2,'LineWidth',1.8); 
set(findobj(gcf,'type','axes'),'FontName','Helvetica','FontSize',14.5,'FontWeight','Bold', 'LineWidth', 2)
% set(gca,'XTick',ind);
% set(gca,'XTickLabel',{'0.2','0.4','0.6','0.8','1','1.2','1.4','1.6','1.8'})
xticklabels({'0.2','0.4','0.6','0.8','1.0','1.2','1.4','1.6','1.8','10^{10}'})

set(get(gca,'YLabel'),'Rotation',0)
y = ylabel('\epsilon_{N}','FontSize',23);

% adjust position of y label
t = title('DBLP');
dy = 0.0; % horizontal offset. Adjust manually
tpos = get(t, 'Position');
ypos = get(y, 'Position');
set(y, 'Position', [ypos(1)+dy tpos(2) ypos(3)], 'Rotation', 0)
xlabel('\lambda','FontSize',23);
hold off
disp('a')

print('plots/line_chart/DBLP_EN','-depsc');
end

