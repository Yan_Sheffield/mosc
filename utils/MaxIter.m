function t_max = MaxIter(G, phi)
% Input:
% - G: the adjacency matrix of the given network.
% - phi: the user-defined conductance upper-bound of the desired graph cut. 
% Output:
% - t_max: the estimated maximum iteration number. 

c1=200;
mu_V = full(sum(sum(G)));
l = log2(mu_V)/2;
t1 = 2/phi^2*log(c1*(l+2)*sqrt(mu_V/2));
t_max = (l+1)*t1;