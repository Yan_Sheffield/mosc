function errors = computeError(actual,ground_truth)
    % Compute the subspace estimation error and the clustering error
    %
    % A  : N estimated labels of the N samples 
    % A0 : N true labels
    %
    if size(actual,2) == 1
        actual = actual';
    end
    
    if size(ground_truth,2) == 1
        ground_truth = ground_truth';
    end
    
    L  = max(actual);% L is the maximum actual group number
    L0 = max(ground_truth);% L0 is the maximum ground-truth group number
    
    N = numel(actual); % the total number of elements in A
    
    Aerror = zeros(L,max(L,L0)); % Aerror is the all possible permutation results
    % compare with actual value
    for i=1:L % L is the maximum actual group number
        % compare with ground_truth
        for j=1:L0 % L0 is the maximum ground-truth group number
            % actual == i: the logical index that actual belongs to group one/two...
            % ground_truth(): index belongs to ground_truth
            % ~= j: whether is ground_trut's 1/2(j).. group
            Aerror(i,j) = nnz(ground_truth(actual == i) ~= j);% ~= logical operation, return 1 if uneuqal 0 means equal
        end
        Aerror(i,L0+1:end) = nnz(actual == i);
    end
    
    if max(L,L0) <= 10
        % for <=10 labels, compute error for every permutation
        perm = perms(1:max(L,L0)); % perms() is all possible permutations
        perm = perm(:,1:L);

        ind_set = repmat((0:L-1)*L,size(perm,1),1) + perm; % repmat() repeat copy of array
        [errors,~]  = min(sum(Aerror(ind_set),2));  % Find the best permutation of label indices that minimize the disagreement
        errors = errors / N;

    else
        % for >10 labels, compute approximate error using hill climbing
        % assumed L=L0
        swap = [];
        for i=2:L
            swap = [swap; repmat(i,i-1,1) (1:i-1)'];
        end
        
        errors = N;
        for i=1:10
            perm = randperm(L);
            
            for j=1:1e5
                [m,ind] = min(Aerror(sub2ind(size(Aerror),swap(:,1),perm(swap(:,2))')) ...
                            + Aerror(sub2ind(size(Aerror),swap(:,2),perm(swap(:,1))')) ...
                            - Aerror(sub2ind(size(Aerror),swap(:,1),perm(swap(:,1))')) ...
                            - Aerror(sub2ind(size(Aerror),swap(:,2),perm(swap(:,2))')));
                
                if m >= 0 break; end
                
                temp = perm(swap(ind,1));
                perm(swap(ind,1)) = perm(swap(ind,2));
                perm(swap(ind,2)) = temp;
            end
            
            perm
            sum(Aerror(sub2ind(size(Aerror),1:L,perm)))
            
            if sum(Aerror(sub2ind(size(Aerror),1:L,perm))) < errors
                errors = sum(Aerror(sub2ind(size(Aerror),1:L,perm)));
            end
        end
        
        errors = errors / N;
        
    end
    
end