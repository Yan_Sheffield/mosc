function [P] = add_dangling_vector(G,P,vec)
    ind_zero = find(~vec); % this is index that sum = 0 TODO:add a condition
    node_num = size(G,1); % total number of nodes
    % create dangling vetors and give them to entry with zero in P
    entity_dangling(1:node_num,1) = 1./node_num;
    zz_num_coloum = size(ind_zero,2);
    entity_dangling = repmat(entity_dangling,1,zz_num_coloum);
    P(:,ind_zero) = entity_dangling;
end
