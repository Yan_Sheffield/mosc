% youtube error node 
% Y_EN_20 = [1.90, 2.43,12.54,20.04,6.90,1.32;2.41,2.46,13.77, 22.35,9.48,1.49];
% b = bar(Y_EN_20);
% title('mis-clustered nodes','FontSize',18)
% % legend('SC [2]','norm-SC [3]','TTM [6]','HSC [5]','TSC [4]','MSC','Location','northwest')
% ylim([0 14])
% print('plots/bar_chart/youtube_err_node','-depsc');

Y_ET = [17.02,16.04,36.17,298.23, 23.19,3.65;
        10.61,12.03,26.67,238.74, 23.56,4.04];
b = bar(Y_ET);
title('mis-clustered triangles','FontSize',18)
% legend('SC','norm-SC','TTM','HSC','TSC','MSC','Location','northwest')
ylim([0 40])
print('plots/bar_chart/youtube_err_triangle','-depsc');

% youtube error triangle
% Y_ET_20 = [23.69,9.55,23.12,6.30,3.78];
% b = bar(Y_ET_20,'FaceColor','flat');
% b.FaceColor = 'flat';
% b.CData(5,:) = [.5 0 .5];
% title('mis-clustered triangles','FontSize',18)
% print('plots/bar_chart/youtube_err_triangle','-depsc');

% LJ error node
% LJ_EN_20 = [5.28,3.21,5.62,3.43,2.77];
% b = bar(LJ_EN_20,'FaceColor','flat');
% b.FaceColor = 'flat';
% b.CData(5,:) = [.5 0 .5];
% title('mis-clustered nodes','FontSize',18)
% print('plots/bar_chart/LJ_err_node','-depsc');

% LJ error triangle
% LJ_ET_20 = [22107.37,6695.91,22168.01,4778.87,416.28];
% b = bar(LJ_ET_20,'FaceColor','flat');
% b.FaceColor = 'flat';
% b.CData(5,:) = [.5 0 .5];
% title('mis-clustered triangles','FontSize',18)
% print('plots/bar_chart/LJ_err_trinagle','-depsc');
