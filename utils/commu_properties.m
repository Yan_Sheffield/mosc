function commu_properties(dataset,max_node_num,top_num)
addpath('utils/')
addpaths();
load(['data/real_world/ground_truth_matrix/com_' dataset '.mat']);
load(['data/real_world/adj_matrix/full_adj_matrix/com_' dataset '.mat']);
if strcmp(dataset, 'dblp')
    adj = com_dblp;
    GT_matrix = com_dblp_top5000_GT_matrix;
end

if strcmp(dataset, 'youtube')
    adj = com_youtube;
    GT_matrix = com_youtube_top5000_GT_matrix;
end

if strcmp(dataset, 'LJ')
    adj = com_IJ;
    GT_matrix = com_lj_top5000_GT_matrix;
end

if strcmp(dataset,'orkut')
    adj = com_orkut;
    GT_matrix = com_orkut_top5000_GT_matrix;
end

adj = adj+ adj.';
num_communities = 5000;


% [node_num,edge_num,triangle_num] = single_properties(adj,GT_matrix,num_communities);
% edge_density = edge_num./node_num;
% triangle_density = triangle_num./node_num;
% index = (1:num_communities)';
% T_single_commu_properties_orkut = table(index,node_num,edge_num,triangle_num,edge_density,triangle_density);
% disp(T_single_commu_properties_dblp)
% 
% save('/Users/geyan/Documents/MATLAB/hypergraph_methods/clustering/data/commu_property/ORKUT/T_single_commu_properties.mat','T_single_commu_properties_orkut');
% disp('run pairwise')

[pair_indx,total_nodes,total_edges,edge_connections] = pairwise_communities(num_communities,GT_matrix,adj,max_node_num);
T_pair_commu_properties = table(pair_indx,total_nodes,total_edges,edge_connections);
% delete empty rows
toDelete = strcmp(T_pair_commu_properties.pair_indx,"");
T_pair_commu_properties(toDelete,:) = [];
save(['data/commu_property/' upper(dataset) '/T_pair_commu_properties_' int2str(max_node_num) '.mat'],'T_pair_commu_properties');

% get a commnunity with medium connections with one top communnity ranked by triangle density
% filter_top_connections(dataset,max_node_num,top_num)
end

function filter_top_connections(dataset,max_node_num,top_num)
% only for LJ, number is 100;
load(['data/commu_property/' upper(dataset) '/T_pair_commu_properties_' int2str(max_node_num) '.mat'])
load(['data/commu_property/' upper(dataset) '/T_sorted_triangle_density_' dataset '.mat'])

pair_indx = strings(top_num,1);
edge_connections = zeros(top_num,1);
total_nodes = zeros(top_num,1);
total_edges = zeros(top_num,1);
T_connections = table(pair_indx,total_nodes,total_edges,edge_connections);


all_pair_index = T_pair_commu_properties{:,{'pair_indx'}};
% split pair
all_pair_index = split(all_pair_index,'_');
row_num = 1;
density_index = 1;
while row_num <= top_num
    % extract nth community sorted with TPR
    index = T_sorted_triangle_density_LJ{density_index,{'index'}};
    % find community in community pairwise
    IndexC = strcmp(all_pair_index,num2str(index));
    % no pairwise for this top triangle community
    if nnz(IndexC)==0
        density_index = density_index+1;
        continue;
    end
     % column_array is the columns containing index
    row_array = find(any(IndexC,2));
    
    % extract all communities that connect with 'index' community
    T_all_connections = T_pair_commu_properties(row_array,:);
    % sort table with 
    T_all_connections = sortrows(T_all_connections,4,'descend');
    % medium row number
%     medium_row = round(height(T_all_connections)/2);
    % the first row has largest connections
    max_row = 1;
    % check duplicated ones
    duplicated = strfind(T_connections{:,{'pair_indx'}},T_all_connections{max_row,{'pair_indx'}});
    % non duplicated
    if isempty(find(not(cellfun('isempty', duplicated))))
        T_connections(row_num,:) = T_all_connections(max_row,:);
        row_num = row_num + 1;
        density_index = density_index+1;
    else
        density_index = density_index+1;
        continue;
    end
    save(['data/commu_property/' upper(dataset) '/max/T_connections_' int2str(top_num) '.mat'],'T_connections')
end
end

function[pair_indx,total_nodes,total_edges,edge_connections] = pairwise_communities(num_communities,GT_matrix,adj,max_node_num)
pairwise_combins = combnk(1:num_communities,2);

size_pairwise_combins = size(pairwise_combins,1);
% all_combinations = zeros(size_pairwise_combins,1);
pair_indx = strings(size_pairwise_combins,1);
total_nodes = zeros(size_pairwise_combins,1);
total_edges = zeros(size_pairwise_combins,1);
% triangle_density = zeros(size_pairwise_combins,1);
edge_connections = zeros(size_pairwise_combins,1);

for i = 1:size_pairwise_combins
    % group index
    cluster_ind_1 = pairwise_combins(i,1);
    cluster_ind_2 = pairwise_combins(i,2);
    % labels in ground truth
    node_label_1 = GT_matrix(cluster_ind_1,find(GT_matrix(cluster_ind_1,:)))+1;
    node_label_2 = GT_matrix(cluster_ind_2,find(GT_matrix(cluster_ind_2,:)))+1;
    % each size of communitu should be smaller than 200(dblp, youtube,orkut_20)
    % 100 (orkut_50),LJ
    node_size_1 = size(node_label_1,2);
    node_size_2 = size(node_label_2,2);
    if node_size_1 > max_node_num || node_size_2 > max_node_num
        continue;
    end
    % edges within group one and two
    edges_cluster_1 = nnz(adj(node_label_1,node_label_1))./2;
    edges_cluster_2 = nnz(adj(node_label_2,node_label_2))./2;
    total_edges_temp = nnz(adj([node_label_1 node_label_2],[node_label_1 node_label_2]))./2;
    edge_connections_temp = total_edges_temp - edges_cluster_1 - edges_cluster_2;
    if edge_connections_temp == 0
        continue;
    end

    % two group names
    pair_indx(i) =sprintf('%d_%d',cluster_ind_1,cluster_ind_2);
    total_nodes(i) = node_size_1 + node_size_2;
    total_edges(i) = total_edges_temp;
    edge_connections(i) = edge_connections_temp;
 
%     [triangle_cluster_1] = TP_Triangle(adj(node_label_1,node_label_1));
%     [triangle_cluster_2] = TP_Triangle(adj(node_label_2,node_label_2));
%     triangle_density(i) = (nnz(triangle_cluster_1) ./6 + nnz(triangle_cluster_2)./6)/(node_size_1+node_size_2);

end
end

function[node_num,edge_num,triangle_num] = single_properties(adj,GT_matrix,num_communities)
node_num = zeros(num_communities,1);
edge_num = zeros(num_communities,1);
triangle_num = zeros(num_communities,1);
for i = 1:num_communities
%     disp(i);
    node_label = GT_matrix(i,find(GT_matrix(i,:)))+1;
    % num node array
    node_num(i) = size(node_label,2);
    plant_model = adj(node_label,node_label);
    % num edges array
    edge_num(i) = nnz(plant_model)/2;
    % TODO: need to verify tensor adj matrix
    [P_no_seloop] = TP_Triangle(plant_model);
    % num triangle array %TODO:plot it and verify it
    triangle_num(i) = nnz(P_no_seloop)/6;
end
end



% function trial_large_community()
% load('data/commu_property/DBLP/T_sorted_node_dblp_trailed.mat');
% toDelete = T_sorted_node_dblp.node_num>200;
% T_sorted_node_dblp(toDelete,:) = [];
% save('data/commu_property/DBLP/T_sorted_node_dblp_trailed.mat','T_sorted_node_dblp')
% end