function [labelled_set] = label_nodes(C,n_node)
    % this only suits for bipartition
    labelled_set = ones(1,n_node);
    labelled_set(C) = 2;
    labelled_set = labelled_set';
end

