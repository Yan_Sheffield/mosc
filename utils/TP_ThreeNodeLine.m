function P = TP_ThreeNodeLine(G)

n = size(G,1);
G = G + speye(n);
P = sparse(n,n*n);
for k = 1:n
    temp = (repmat(G(k,:),n,1).*G);
    P(:,(k-1)*n+1:k*n) = temp;
end