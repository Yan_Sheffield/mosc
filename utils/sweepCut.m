function [best_cut_conduc,best_cut_expen,best_cut_dens,best_cut_nassoc,best_cut_ncut,best_cut_min,best_cut_before_refined,best_cut_before_refined_nmi,best_labelled_conduc,best_labelled_min,actual_labelled_cut_tri,actual_labelled_cut_edge,triangle_array,edge_array,t_sweep_total] = sweepCut(G,P_no_seloop,n_node,cut_order,total_motifs,true_label,isrefined,isrealData,isHCut,algType,motif_adj,lambda)
best_cluster_conduc = NaN; % TODO?1. combine return value of sweep cut 2. indicator vector for ground truth
best_cut_conduc = NaN;
best_assoc_S_conduc = NaN;
best_assoc_S_bar_conduc = NaN;
 
best_cluster_expen = NaN;
best_cut_expen = NaN;
best_assoc_S_expen = NaN;
best_assoc_S_bar_expen = NaN;
 
best_cluster_ncut = NaN;
best_cut_ncut = NaN;
best_assoc_S_ncut = NaN;
best_assoc_S_bar_ncut = NaN;
 
best_cluster_min = NaN;
best_cut_min = NaN;
best_assoc_S_min = NaN;
best_assoc_S_bar_min = NaN;
 
best_cluster_dens = NaN;
best_cut_dens = NaN;
best_assoc_S_dens = NaN;
best_assoc_S_bar_dens = NaN;
 
best_cluster_nassoc = NaN;
best_cut_nassoc = NaN;
best_assoc_S_nassoc = NaN;
best_assoc_S_bar_nassoc = NaN;
% 
best_cluster_optimalErr = NaN;
best_cut_optimalErr = NaN;
best_assoc_S_optimalErr = NaN;
best_assoc_S_bar_optimalErr = NaN;
 
best_cluster_optimalErr_nmi = NaN;
best_cut_optimalErr_nmi = NaN;
best_assoc_S_optimalErr_nmi = NaN;
best_assoc_S_bar_optimalErr_nmi = NaN;

best_cluster_optimalErrTri = NaN;
best_cut_optimalErrTri = NaN;
best_assoc_S_optimalErrTri = NaN;
best_assoc_S_bar_optimalErrTri = NaN;
 
best_cluster_optimalErrEdge = NaN;
best_cut_optimalErrEdge = NaN;
best_assoc_S_optimalErrEdge = NaN;
best_assoc_S_bar_optimalErrEdge = NaN;
 
% err_conduc = NaN;
% err_expen = NaN;
% err_ncut = NaN;
% err_min = NaN;
% err_dens = NaN;
% err_nassoc = NaN;
 
best_cut_before_refined = NaN;
t_sweep_total = zeros(3,1);
 
for j = 1:n_node-1 % Sweep cuts
    C = cut_order(1:j)';
    C_bar = cut_order(j+1:n_node)';
    % this P is the real P due to excluding dangling vector
    % we need evolved conduc,expension.. to plot for each cut
    if strcmp(isHCut, '1')
        
        [conduc,expension,density,nassoc,ncut,conduc_mix,optimal_err,optimal_nmi,optimal_err_triangle,optimal_err_edge,assoc_S,assoc_S_bar,t_conduct_3,t_nassoc_3,t_conduc_mix]  = HCut(G,P_no_seloop,C,C_bar,n_node,total_motifs,'Triangle','undirected',true_label,motif_adj,lambda);
        t_sweep_total(1) = t_sweep_total(1) + t_conduct_3;
        t_sweep_total(2) = t_sweep_total(2) + t_nassoc_3;
        t_sweep_total(3) = t_sweep_total(3) + t_conduc_mix;
        
        % only calcuate optimal error for once % add optimal edge bigger
        [best_cluster_optimalErr,best_cut_optimalErr,best_assoc_S_optimalErr,best_assoc_S_bar_optimalErr] = filter_Cluster(optimal_err,C,best_cluster_optimalErr,best_cut_optimalErr,'optimalErr',j,assoc_S,assoc_S_bar,best_assoc_S_optimalErr,best_assoc_S_bar_optimalErr);
        % the better for NMI
        [best_cluster_optimalErr_nmi,best_cut_optimalErr_nmi,best_assoc_S_optimalErr_nmi,best_assoc_S_bar_optimalErr_nmi] = filter_Cluster(optimal_nmi,C,best_cluster_optimalErr_nmi,best_cut_optimalErr_nmi,'optimalErr_nmi',j,assoc_S,assoc_S_bar,best_assoc_S_optimalErr_nmi,best_assoc_S_bar_optimalErr_nmi);

        % optimal triangle cut
        [best_cluster_optimalErrTri,best_cut_optimalErrTri,best_assoc_S_optimalErrTri,best_assoc_S_bar_optimalErrTri] = filter_Cluster(optimal_err_triangle,C,best_cluster_optimalErrTri,best_cut_optimalErrTri,'optimalErr',j,assoc_S,assoc_S_bar,best_assoc_S_optimalErrTri,best_assoc_S_bar_optimalErrTri);
        % optimal edge cut
        [best_cluster_optimalErrEdge,best_cut_optimalErrEdge,best_assoc_S_optimalErrEdge,best_assoc_S_bar_optimalErrEdge] = filter_Cluster(optimal_err_edge,C,best_cluster_optimalErrEdge,best_cut_optimalErrEdge,'optimalErr',j,assoc_S,assoc_S_bar,best_assoc_S_optimalErrEdge,best_assoc_S_bar_optimalErrEdge);
    else
        [conduc,expension,density,nassoc,ncut,conduc_mix,assoc_S,assoc_S_bar,t_conduct_2,t_nassoc_2,t_ncut_2]  = ECut(G,P_no_seloop,C,C_bar,n_node,'undirected');
        t_sweep_total(1) = t_sweep_total(1) + t_conduct_2;
        t_sweep_total(2) = t_sweep_total(2) + t_nassoc_2;
        t_sweep_total(3)= t_sweep_total(3) + t_ncut_2;
    end
    
    [best_cluster_expen,best_cut_expen,best_assoc_S_expen,best_assoc_S_bar_expen] = filter_Cluster(expension,C,best_cluster_expen,best_cut_expen,'expen',j,assoc_S,assoc_S_bar,best_assoc_S_expen,best_assoc_S_bar_expen);
    
    [best_cluster_conduc,best_cut_conduc,best_assoc_S_conduc,best_assoc_S_bar_conduc] = filter_Cluster(conduc,C,best_cluster_conduc,best_cut_conduc,'conduc',j,assoc_S,assoc_S_bar,best_assoc_S_conduc,best_assoc_S_bar_conduc);
    
    [best_cluster_ncut,best_cut_ncut,best_assoc_S_ncut,best_assoc_S_bar_ncut] = filter_Cluster(ncut,C,best_cluster_ncut,best_cut_ncut,'ncut',j,assoc_S,assoc_S_bar,best_assoc_S_ncut,best_assoc_S_bar_ncut);
    
    [best_cluster_min,best_cut_min,best_assoc_S_min,best_assoc_S_bar_min] = filter_Cluster(conduc_mix,C,best_cluster_min,best_cut_min,'min',j,assoc_S,assoc_S_bar,best_assoc_S_min,best_assoc_S_bar_min);
    
    [best_cluster_dens,best_cut_dens,best_assoc_S_dens,best_assoc_S_bar_dens] = filter_Cluster(density,C,best_cluster_dens,best_cut_dens,'dens',j,assoc_S,assoc_S_bar,best_assoc_S_dens,best_assoc_S_bar_dens);
    
    [best_cluster_nassoc,best_cut_nassoc,best_assoc_S_nassoc,best_assoc_S_bar_nassoc] = filter_Cluster(nassoc,C,best_cluster_nassoc,best_cut_nassoc,'nassoc',j,assoc_S,assoc_S_bar,best_assoc_S_nassoc,best_assoc_S_bar_nassoc);
    
end
 
 
% we don't need plot precision, recall for each cut % TODO: the below block
% of codes can be capsulized.
best_labelled_conduc = label_nodes(best_cluster_conduc,n_node);
best_labelled_expen = label_nodes(best_cluster_expen,n_node);
best_labelled_ncut = label_nodes(best_cluster_ncut,n_node);
best_labelled_min = label_nodes(best_cluster_min,n_node);
 
% best_labelled_dens = label_nodes(best_cluster_dens,n_node);
best_labelled_nassoc = label_nodes(best_cluster_nassoc,n_node);
if strcmp(isHCut, '1')
    % metric: these label is used for calculating error triangle
    best_labelled_optimalErr_Tri = label_nodes(best_cluster_optimalErrTri,n_node);
    actual_labelled_cut_tri = [best_labelled_conduc best_labelled_expen best_labelled_nassoc best_labelled_ncut best_labelled_min best_labelled_optimalErr_Tri];
    
    best_labelled_optimalErr_edge = label_nodes(best_cluster_optimalErrEdge,n_node);
    actual_labelled_cut_edge = [best_labelled_conduc best_labelled_expen best_labelled_nassoc best_labelled_ncut best_labelled_min best_labelled_optimalErr_edge];
% remove all calulation about triangle and edge density
    %     if strcmp(algType,'Hybrid')||strcmp(algType,'MOSC2')
%         % caluclate triangle density when performing triangle cut criteria
%         % we know the relationship between assoc_3 and the num of
%         % triangle(assoc_3 / 3), so we don't need adj tensor.
%         conduc_triangle = calcu_triangle_3(best_cluster_conduc,best_assoc_S_conduc,best_assoc_S_bar_conduc,n_node);
%         expen_triangle = calcu_triangle_3(best_cluster_expen,best_assoc_S_expen,best_assoc_S_bar_expen,n_node);
%         nassoc_triangle = calcu_triangle_3(best_cluster_nassoc,best_assoc_S_nassoc,best_assoc_S_bar_nassoc,n_node);
%         ncut_triangle = calcu_triangle_3(best_cluster_ncut,best_assoc_S_ncut,best_assoc_S_bar_ncut,n_node);
%         min_triangle = calcu_triangle_3(best_cluster_min,best_assoc_S_min,best_assoc_S_bar_min,n_node);
%     end
else
    actual_labelled_cut_tri = [best_labelled_conduc best_labelled_expen best_labelled_nassoc best_labelled_ncut best_labelled_min];
    actual_labelled_cut_edge = [best_labelled_conduc best_labelled_expen best_labelled_nassoc best_labelled_ncut best_labelled_min];
%     if strcmp(algType,'Hybrid')||strcmp(algType,'MOSC2')
%         % caluclate triangle density when performing edge cut criteria
%         conduc_triangle = calcu_triangle_2(best_cluster_conduc,cut_order,P_no_seloop,n_node);
%         expen_triangle = calcu_triangle_2(best_cluster_expen,cut_order,P_no_seloop,n_node);
%         nassoc_triangle = calcu_triangle_2(best_cluster_nassoc,cut_order,P_no_seloop,n_node);
%         ncut_triangle = calcu_triangle_2(best_cluster_ncut,cut_order,P_no_seloop,n_node);
%         min_triangle = calcu_triangle_2(best_cluster_min,cut_order,P_no_seloop,n_node);
%     end
end
% calculate edge density
% if strcmp(algType,'Hybrid')||strcmp(algType,'MOSC2')
%     conduc_edge = cal_edge(best_cluster_conduc,cut_order,G,n_node);
%     expen_edge = cal_edge(best_cluster_expen,cut_order,G,n_node);
%     nassoc_edge = cal_edge(best_cluster_nassoc,cut_order,G,n_node);
%     ncut_edge = cal_edge(best_cluster_ncut,cut_order,G,n_node);
%     min_edge = cal_edge(best_cluster_min,cut_order,G,n_node);
% end
 
% we don't calculate error rate for each cut if data is from real world.
% we only calculate error after finishing all cuts.
if isrealData == 0
    % calculate error nodes
    err_conduc = computeError(best_labelled_conduc,true_label);
    err_expen = computeError(best_labelled_expen,true_label);
    err_ncut = computeError(best_labelled_ncut,true_label);
    err_min = computeError(best_labelled_min,true_label);
%     err_dens = computeError(best_labelled_dens,true_label);
    err_nassoc = computeError(best_labelled_nassoc,true_label);
    
    
    
    err_conduc_nmi = nmi(best_labelled_conduc,true_label);
    err_expen_nmi = nmi(best_labelled_expen,true_label);
    err_ncut_nmi = nmi(best_labelled_ncut,true_label);
    err_min_nmi = nmi(best_labelled_min,true_label);
%     err_dens = computeError(best_labelled_dens,true_label);
    err_nassoc_nmi = nmi(best_labelled_nassoc,true_label);
    % these cuts are not affected by refinement method
    if strcmp(isHCut, '1')
        best_cut_before_refined = [err_conduc;err_expen;err_nassoc;err_ncut;err_min;best_cut_optimalErr];
        best_cut_before_refined_nmi = [err_conduc_nmi;err_expen_nmi;err_nassoc_nmi;err_ncut_nmi;err_min_nmi;best_cut_optimalErr_nmi];
%         if strcmp(algType,'Hybrid')||strcmp(algType,'MOSC2')
%             triangle_array = [conduc_triangle;expen_triangle;nassoc_triangle;ncut_triangle;min_triangle];
%             edge_array = [conduc_edge;expen_edge;nassoc_edge;ncut_edge;min_edge];
%         else
%             triangle_array = [0;0;0;0;0];
%             edge_array = [0;0;0;0;0];
%         end
    else
        best_cut_before_refined = [err_conduc;err_expen;err_nassoc;err_ncut;err_min];
        best_cut_before_refined_nmi = [err_conduc_nmi;err_expen_nmi;err_nassoc_nmi;err_ncut_nmi;err_min_nmi];
%         if strcmp(algType,'Hybrid')||strcmp(algType,'MOSC2')
%             triangle_array = [conduc_triangle;expen_triangle;nassoc_triangle;ncut_triangle;min_triangle];
%             edge_array = [conduc_edge;expen_edge;nassoc_edge;ncut_edge;min_edge];
%         else
%             triangle_array = [0;0;0;0;0];
%             edge_array = [0;0;0;0;0];
%         end
    end
    triangle_array = [0;0;0;0;0];
    edge_array = [0;0;0;0;0];
end
 
if strcmp(isrefined, '1')
    % Perhaps clusters have been changed from conductance sweep reslut
    % due to swap some nodes. Exploring how triangle refinement imporves
    % serval defined criteria, and re-calculate conduc and error nodes for
    % each metirc.
    
    % the below maybe useful in the futher. % That maybe useless for real
    % data
    %     [best_cut_conduc,err_conduc] = refinement_process(G,total_motifs,P_no_seloop,true_label,best_cluster_conduc,n_node,best_assoc_S_conduc,best_assoc_S_bar_conduc,'conduc');
    %     [best_cut_expen,err_expen] = refinement_process(G,total_motifs,P_no_seloop,true_label,best_cluster_expen,n_node,best_assoc_S_expen,best_assoc_S_bar_expen,'expen');
    %     [best_cut_dens,err_dens] = refinement_process(G,total_motifs,P_no_seloop,true_label,best_cluster_dens,n_node,best_assoc_S_dens,best_assoc_S_bar_dens,'dens');
    %     [best_cut_nassoc,err_nassoc] = refinement_process(G,total_motifs,P_no_seloop,true_label,best_cluster_nassoc,n_node,best_assoc_S_nassoc,best_assoc_S_bar_nassoc,'nassoc');
    %     [best_cut_ncut,err_ncut] = refinement_process(G,total_motifs,P_no_seloop,true_label,best_cluster_ncut,n_node,best_assoc_S_ncut,best_assoc_S_bar_ncut,'ncut');
    [best_cut_min,err_min] = refinement_process(G,total_motifs,P_no_seloop,true_label,best_cluster_min,n_node,best_assoc_S_min,best_assoc_S_bar_min,'min');
end
end
 
function [best_cut,err_nodes] = refinement_process(G,total_motifs,P_no_seloop,true_label,best_cluster,n_node,best_assoc_S,best_assoc_S_bar,type)
swap_best_labelled = triangle_refinement(G,total_motifs,P_no_seloop,best_cluster,n_node,best_assoc_S,best_assoc_S_bar);
[C_swap,C_bar_swap] = grouped_nodes(swap_best_labelled);
err_nodes = computeError(swap_best_labelled,true_label);
% recalculate multiple cuts after refining.
[best_cut_conduc,best_cut_expen,best_cut_dens,best_cut_nassoc,best_cut_ncut,best_cut_min,~,~]  = HCut(G,P_no_seloop,C_swap,C_bar_swap,n_node,total_motifs,'Triangle','undirected',true_label);
if strcmp(type, 'conduc')
    best_cut = best_cut_conduc;
end
if strcmp(type, 'expen')
    best_cut = best_cut_expen;
end
if strcmp(type, 'dens')
    best_cut = best_cut_dens;
end
if strcmp(type, 'nassoc')
    best_cut = best_cut_nassoc;
end
if strcmp(type, 'ncut')
    best_cut = best_cut_ncut;
end
if strcmp(type, 'min')
    best_cut = best_cut_min;
end
 
end
 
function[C,C_bar] = grouped_nodes(cluster_indices)
[~,C,~] = find(cluster_indices==1);
[~,C_bar,~] = find(cluster_indices==2);
end
 
function [best_cluster,best_cut,best_assoc_S,best_assoc_S_bar] = filter_Cluster(Cut_value,C,best_cluster,best_cut,cut_Type,j,assoc_S,assoc_S_bar,best_assoc_S,best_assoc_S_bar )
 
if j == 1
    best_cut = Cut_value;
    best_cluster = C;
    best_assoc_S = assoc_S;
    best_assoc_S_bar = assoc_S_bar;
else  % it suits for criteria that are smaller the better
    if (best_cut >= Cut_value) && (strcmp(cut_Type, 'conduc')||strcmp(cut_Type, 'expen')||strcmp(cut_Type, 'ncut')||strcmp(cut_Type, 'min')||strcmp(cut_Type, 'optimalErr'))
        best_cut = Cut_value;
        best_cluster = C;
        best_assoc_S = assoc_S;
        best_assoc_S_bar = assoc_S_bar;
    end
    if (best_cut <= Cut_value) && (strcmp(cut_Type, 'dens')||strcmp(cut_Type, 'nassoc')||strcmp(cut_Type, 'optimalErr_nmi'))
        best_cut = Cut_value;
        best_cluster = C;
        best_assoc_S = assoc_S;
        best_assoc_S_bar = assoc_S_bar;
    end
end
end
 
% calculate triangle density for third-order cut type
function[triangle_density] = calcu_triangle_3(best_cluster,best_assoc_S,best_assoc_S_bar,num_node)
n_node_cluster = size(best_cluster,2);
n_node_cluster_bar = num_node - n_node_cluster;
 
num_triangle_S = best_assoc_S/3;
num_triangle_S_bar = best_assoc_S_bar/3;
 
triangle_density = num_triangle_S/n_node_cluster+num_triangle_S_bar/n_node_cluster_bar;
end
 
% calculate triangle density for second-order cut type
function [triangle_density] = calcu_triangle_2(best_cluster,cut_order,P_no_seloop,num_node)
n_node_cluster = size(best_cluster,2);
n_node_cluster_bar = num_node - n_node_cluster;
 
best_cluster_S_bar = cut_order(n_node_cluster+1:num_node)';
 
num_triangle_S = count_triangles(best_cluster,P_no_seloop,num_node);
 
num_triangle_S_bar = count_triangles(best_cluster_S_bar,P_no_seloop,num_node);
 
triangle_density = num_triangle_S/n_node_cluster+num_triangle_S_bar/n_node_cluster_bar;
end
 
 
function[num_triangles] = count_triangles(C,P_no_seloop,node_num)
 % cluster size 
    n_c = size(C,2); 
    temp = zeros(1, n_c*n_c);
    % calculate all about columns about nodes in one cluster eg.P(C,C)
    for i = 1: n_c
        temp((i-1)*n_c+1:i*n_c) = (C(i)-1)*node_num + C; 
    end
     num_triangles = nnz(P_no_seloop(C, temp))/6;
end
 
function [edge_density] = cal_edge(best_cluster,cut_order,G,num_node)
n_node_cluster = size(best_cluster,2);
n_node_cluster_bar = num_node - n_node_cluster;
best_cluster_S_bar = cut_order(n_node_cluster+1:num_node)';
 
edge_number = nnz(G(best_cluster,best_cluster))./2;
edge_number_bar = nnz(G(best_cluster_S_bar,best_cluster_S_bar))./2;
 
denominator_cluster = (n_node_cluster*(n_node_cluster-1))*1/2;
denominator_cluster_bar = (n_node_cluster_bar*(n_node_cluster_bar-1))*1/2;
 
if denominator_cluster == 0
    edge_density_cluster = 0;
else
    edge_density_cluster = edge_number/denominator_cluster;
end
 
if denominator_cluster_bar == 0
    edge_density_cluster_bar = 0;
else
    edge_density_cluster_bar = edge_number_bar/denominator_cluster_bar;
end
 
edge_density = edge_density_cluster + edge_density_cluster_bar;
end
 

