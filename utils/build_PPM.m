function [plant_model,true_labels] = build_PPM(cluster_size,num_clusters,p_intra,q_inter)
% s(i_n): the number of node in one cluster,m(i_exp): the order,k: 2 clusters,p,q is the probability
n = cluster_size*num_clusters;
true_labels = zeros(n,1);
for i = 1:num_clusters
    true_labels((i-1)*cluster_size + (1:cluster_size)) = i;
end

plant_model = zeros(n*ones(1,2));% 10*10 matrix if m = 2;10*10*10 real tensor if m = 3
% H = zeros(n,nchoosek(n,2));% nchoosek is the total combination
% Hcounter = 0;

for i1 = 1:n
    for i2 = i1+1:n % iterate all of pairwise combinations with non-repeatitively
        % sometimes it has self-loop. it's consistent with defination of PPM
        if (min(true_labels([i1 i2]))==max(true_labels([i1 i2])))% that means i1 i2 should be in the same group
            prob = p_intra; % the actual probability that they are connected
        else
            prob = q_inter;
        end
        if (rand(1)>prob)
            continue 
        end
%         Hcounter = Hcounter + 1;
%         H([i1 i2],Hcounter) = 1;% the coloum is maximum combinations
        % we only consider undirected graph
        plant_model(i1,i2) = 1;
        plant_model(i2,i1) = 1;
        continue
    end
end

%%%%%% TTM (G&D, arxiv:1606.06516)
% [~,~,idx] = TTM(A,k,m,n,0);
%[~,~,idx] = SpectralClustering(A,2,2);
% phi = 0.5;
% t_max = MaxIter(A, phi);
% [~,~,idx] = HOSLOC(A,1,phi,t_max,3,1,'Triangle',n);

