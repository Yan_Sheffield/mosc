function [refined_label] = triangle_refinement(G,total_motifs,P_no_seloop,best_cluster,n_node,assoc_c,assoc_c_bar)
refined_label = repmat(-1,1,n_node);
refined_label(best_cluster) = 1;% its associativity is assoc_c

result = zeros(1,n_node);
for j = 1:n_node % demension reduction from unfolding tensor to a matrix
    cut_node_cluster_c = 0;
    cut_node_cluster_c_bar = 0;
    slice = P_no_seloop(:,(j-1)*n_node+1:j*n_node);
    [row_index,col_index,~] = find(slice);% obtain index of non-zero elements in a sclice
    for q = 1:size(row_index,1)
        node_1 = row_index(q);
        node_2 = col_index(q);
        % j,node_1 and node_2 are in the same cluster, has strong
        % connection with its own cluster
        if min(refined_label([j node_1 node_2])) == max(refined_label([j node_1 node_2]))
            cut_node_cluster_c = cut_node_cluster_c + 1;
        else
            % node_1 and node_2 are in the same cluster but both are
            % seperated with node j
            if (refined_label(node_1) == refined_label(node_2)) && (refined_label(node_1) ~= refined_label(j))
                cut_node_cluster_c_bar = cut_node_cluster_c_bar + 1;
            else
                % two nodes are distributed into two clusters
                cut_node_cluster_c = cut_node_cluster_c + 0.5;
                cut_node_cluster_c_bar = cut_node_cluster_c_bar + 0.5;
            end
        end
    end
    % eg. (1,2,3) and (1,3,2) are the same triangle
    cut_node_cluster_c = cut_node_cluster_c/2;
    cut_node_cluster_c_bar = cut_node_cluster_c_bar/2;
    % cut_node_cluster_c indicates the connection degree with its own
    % cluster. The most important thing is to clarify the connection degree gap between two clusters.
    % The minus order is different just because the indicator is inversed(positive or negtive)
    
    if refined_label(j) == -1
         % if any assoc is 0, swap this node.Paper did not mention it but
         % it is reasonable
        if assoc_c == 0 || assoc_c_bar == 0
            delta_l = -1;
        else % assoc_c_bar is assoc for -1 cluster
            delta_l = (cut_node_cluster_c /assoc_c_bar) - (cut_node_cluster_c_bar/assoc_c);
        end      
    else
        if assoc_c == 0 || assoc_c_bar == 0 % if any assoc is 0, swap this node
            delta_l = 1;
        else
            delta_l = (cut_node_cluster_c_bar/assoc_c_bar) - (cut_node_cluster_c /assoc_c);
        end        
    end
    
    result(j) = refined_label(j)*delta_l; % here is a bug when cluster node is reversed q =4
    % swap the group if value of result is more than 0
    if result(j) >= 0
        if refined_label(j) == -1
            refined_label(j) = 1;
        else
            refined_label(j) = -1;
        end
        if assoc_c == 0 || assoc_c_bar == 0
            % We need to know updated assoc_c and assoc_c_bar because the
            % nodes won't be swaped if and only if assoc is 0
            [assoc_c,assoc_c_bar] = update_assoc(refined_label,G,P_no_seloop,n_node,total_motifs);
        end
    end
end
k = refined_label== -1;
refined_label(k) = 2;
end

function [assoc_c,assoc_c_bar] = update_assoc(refined_label,G,P_no_seloop,n_node,total_motifs)
 [C,C_bar] = grouped_nodes(refined_label);
 [~,~,~,~,~,~,assoc_c,assoc_c_bar]  = HCut(G,P_no_seloop,C,C_bar,n_node,total_motifs,'Triangle','undirected');
end

% TODO: has a better way to do it
function[C,C_bar] = grouped_nodes(cluster_indices)
[~,C,~] = find(cluster_indices==1);
[~,C_bar,~] = find(cluster_indices==-1);
end

