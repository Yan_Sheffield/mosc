function[W] = sum_unfolding_tensor(unfolding_adj_tensor,n_node)
W = zeros(n_node);
for j = 1:n_node % demension reduction from unfolding tensor to a matrix
    slice = unfolding_adj_tensor(:,(j-1)*n_node+1:j*n_node);
    W = slice + W;
end
end



% function[W,adj_tensor] = sum_unfolding_tensor(unfolding_adj_tensor,n_node)
% W = zeros(n_node);
% adj_tensor = zeros(n_node,n_node,n_node);
% for j = 1:n_node % demension reduction from unfolding tensor to a matrix
%     slice = unfolding_adj_tensor(:,(j-1)*n_node+1:j*n_node);
%     adj_tensor(:,:,j) = slice;
%     W = slice + W;
% end
% disp(issymmetric(adj_tensor));
% end

