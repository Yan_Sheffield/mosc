function p_epi = vec_epi(G,p,epi)

for i = 1:max(size(p,1),size(p,2)) %max(11,1)
    if p(i) >= sum(G(i,:))*epi % epi ensure the thredhold is small enough
       p_epi(i) = p(i);
    else 
       p_epi(i) = 0;
    end
end