function [err_triangle] = error_triangles(actual,ground_truth,P_no_seloop,node_num)
num_diff_cuts = size(actual,2); % 5 different cut types for HCut, 1 is ootimal cut and 5 for ECut
err_triangle = zeros(num_diff_cuts,1);

if size(ground_truth,2) == 1
    ground_truth = ground_truth';
end
% labelling nodes for GT clusters
[GT_one,GT_two] = grouped_nodes(ground_truth);
% extract tensors within one cluster

% GT_one_triangle = adj_tensor(GT_one,GT_one,GT_one);
% GT_two_triangle = adj_tensor(GT_two,GT_two,GT_two);
GT_one_triangle = count_triangles(GT_one,P_no_seloop,node_num);
GT_two_triangle = count_triangles(GT_two,P_no_seloop,node_num);

total_intra_triangles = GT_one_triangle + GT_two_triangle;

for i = 1:num_diff_cuts
    actual_commu = actual(:,i);
    % we do it in case of different directions of arrays
    if size(actual(:,i),2) == 1
        actual_commu = actual_commu';
    end
    
    % labelling nodes for actual clusters
    [actual_one,actual_two] = grouped_nodes(actual_commu);
       
    % matching parallel
    A1_G1 = intersect(actual_one,GT_one);
    A1_G1_triangle = count_triangles(A1_G1,P_no_seloop,node_num);    
    A2_G2 = intersect(actual_two,GT_two);
    A2_G2_triangle = count_triangles(A2_G2,P_no_seloop,node_num);
    
    % matching cross
    A1_G2 = intersect(actual_one,GT_two);
    A1_G2_triangle = count_triangles(A1_G2,P_no_seloop,node_num);    
    A2_G1 = intersect(actual_two,GT_one);
    A2_G1_triangle = count_triangles(A2_G1,P_no_seloop,node_num);
    
    total_parallel = A1_G1_triangle + A2_G2_triangle;
    total_cross = A1_G2_triangle + A2_G1_triangle;
    
    % select more matching clusters as right clusters
    if total_parallel > total_cross
        err_triangle(i) = total_intra_triangles - total_parallel;
        continue 
    end
    
    if total_cross > total_parallel
        err_triangle(i) = total_intra_triangles - total_cross;
        continue 
    end
    
    if total_parallel == total_cross
        % whatever which value is chosen
        err_triangle(i) = total_intra_triangles - total_parallel;
    end
end
end

function[cluster_one,cluster_two] = grouped_nodes(cluster_indices)
[~,cluster_one,~] = find(cluster_indices==1);
[~,cluster_two,~] = find(cluster_indices==2);
end

function[num_triangles] = count_triangles(C,P_no_seloop,node_num)
 % cluster size 
    n_c = size(C,2); 
    temp = zeros(1, n_c*n_c);
    % calculate all about columns about nodes in one cluster eg.P(C,C)
    for i = 1: n_c
        temp((i-1)*n_c+1:i*n_c) = (C(i)-1)*node_num + C; 
    end
     num_triangles = nnz(P_no_seloop(C, temp))/6;
end
