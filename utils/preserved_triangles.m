function [losed_triangles] = preserved_triangles(actual,ground_truth,adj_tensor)
num_diff_cuts = size(actual,2);
losed_triangles = zeros(num_diff_cuts,1);
total_triangles = nnz(adj_tensor)/6;
for i = 1:num_diff_cuts
    actual_commu = actual(:,i);
    % we do it in case of different directions of arrays
    if size(actual(:,i),2) == 1
        actual_commu = actual_commu';
    end
    
    if size(ground_truth,2) == 1
        ground_truth = ground_truth';
    end
    [actual_one,actual_two] = grouped_nodes(actual_commu);
    
    % extract tensors within one cluster
    C1_triangle = adj_tensor(actual_one,actual_one,actual_one);
    C2_triangle = adj_tensor(actual_two,actual_two,actual_two);
    preserved_triangle = (nnz(C1_triangle)+nnz(C2_triangle))/6;
    
    losed_triangles(i) = (total_triangles - preserved_triangle);
end
end


function[cluster_one,cluster_two] = grouped_nodes(cluster_indices)
[~,cluster_one,~] = find(cluster_indices==1);
[~,cluster_two,~] = find(cluster_indices==2);
end