load(['data/Mark_Newman/adj/polblogs_adj.mat']);
load(['data/Mark_Newman/GT/polblogs_ground_truth.mat']);

% load('data/synthetic/2D_adj_matrix/two_triangle_example.mat');
% plant_model = two_triangle_example;
% true_labels = [1 1 1 2 2 2];

% range and size
r_one = randi([1 758],1,379);
r_two = randi([759 1490],1,379);
pairwise_total = [r_one;r_two];
pair_size = size(pairwise_total,2);
node_num = size(adj_matrix,2);
for column_num = 1:pair_size
    pair = pairwise_total(:,column_num);
    first_index = pair(1);
    second_index = pair(2);
    % switch two rows
    adj_matrix([second_index  first_index],:)= adj_matrix([first_index second_index],:);
    % switch ground truth
    ground_truth([second_index first_index]) = ground_truth([first_index second_index]);
    for switch_row = 1:node_num
        if switch_row == first_index || switch_row == second_index
            continue;
        end
        adj_matrix(switch_row,[first_index second_index]) = adj_matrix(switch_row,[second_index first_index]);
    end
end
save('data/Mark_Newman/adj/new_polblogs_adj.mat','adj_matrix');
save('data/Mark_Newman/GT/new_polblogs_ground_truth.mat','ground_truth');
