function plotNetwork(adj,direction,figureName,isHighlight)
% undirected
if direction == 0
    G = graph(adj,'OmitSelfLoops');
else
    G = digraph(adj,'OmitSelfLoops');
end
figureObj = plot(G,'Layout','force');
% highlight nodes
if isHighlight == 1
    highlight(figureObj,[2,4,5,7,9,12,13,16,17,21,22,23,24,25,26,27,28,30,31,32,34,35,36,37,38,40,41,43,45,50,53,54,58,61,62,63,66,67,68,71,74,82,83,84,86,90,93,96,98,101,107,110,111,112],'NodeColor','r');
end
% save figures
saveas(figureObj,['plots/',figureName,'.png'])
end