function [err_SC,err_NMI,err_edge,err_triangle,Result_SC,cut_order_array,t_Lap_eigen_cut] = multi_partition_After_L(L,algType,node_num,k,true_labels,true_label_index,G,total_edges,total_triangles,dataset,P_no_seloop)
disp('start eigs')
tic
eigvecs = sort_eigen_multi_partition(L,algType,node_num,k);
t_cal_eigen = toc;
% MOSCs:for SNAP with multipartions, it only save lambda = 0.5 case; for other
% cases it only svae latest result (lambda = 1).
% save(['result/explore_algs/' upper(dataset) '/eigs/eigvecs_' algType '.mat'],'eigvecs');
disp('start kmeans')
[actual_labelled_set,t_sweep_cut] = run_kmeans(eigvecs,k);
disp('error nodes')
err_SC = computeError(actual_labelled_set',true_labels)*node_num;
disp('start nmi')
err_NMI = nmi(actual_labelled_set',true_labels);
actual_labelled_set = actual_labelled_set';
edge_or_triangle = 'error_edge';
disp('start error edge')
err_edge = multi_partition_error_edge_triangle(actual_labelled_set,true_label_index,G,edge_or_triangle,total_edges);
edge_or_triangle = 'error_triangle';
disp('start error triangle')
err_triangle = multi_partition_error_edge_triangle(actual_labelled_set,true_label_index,G,edge_or_triangle,total_triangles);

% calculate triangle density for auto select lambda for multiple pertitions
true_label_index = trans_label_to_label_matrix(actual_labelled_set,k);
Result_SC = count_intra_edge_triangle(true_label_index,k,P_no_seloop,node_num);

cut_order_array = NaN;
t_Lap_eigen_cut = [t_cal_eigen;t_sweep_cut;zeros(6,1)];
end

 

function true_label_index =  trans_label_to_label_matrix(GT_matrix,num_clusters)
true_label_index = cell(num_clusters,1);
for i = 1:num_clusters
    true_label_index{i} = find(GT_matrix == i);
end
end

% this is for multipartitions
function triangle_density = count_intra_edge_triangle(cluster_label,cluster_No,P_no_seloop,node_num)
num_triangles = 0;
for i = 1:cluster_No
    C = cluster_label{i};
    num_triangle = count_triangles(C,P_no_seloop,node_num);
    num_triangles = num_triangles + num_triangle;
end
triangle_density = num_triangles/node_num;
end
 
function[num_triangles] = count_triangles(C,P_no_seloop,node_num)
 % cluster size 
    n_c = size(C,2); 
    temp = zeros(1, n_c*n_c);
    % calculate all about columns about nodes in one cluster eg.P(C,C)
    for i = 1: n_c
        temp((i-1)*n_c+1:i*n_c) = (C(i)-1)*node_num + C; 
    end
     num_triangles = nnz(P_no_seloop(C, temp))/6;
end

