function [unfolding_adj_tensor,tans_tensor,vec,t_adj_tensor,t_trans_tensor] = transTensor(G)
tic
unfolding_adj_tensor = construct_adj_tensor(G); % P_no_seloop are unfolding adjecency tensors
t_adj_tensor = toc;
%     [P,vec] = construct_RW_tensor(P);
tic
[tans_tensor,vec] = construct_RW_tensor(unfolding_adj_tensor);
t_trans_tensor = toc;
end

function[P,vec] = construct_RW_tensor(P)
    vec = sum(P,1); 
    ind_nnz = find(vec); % this is index that sum ~= 0
    vec(ind_nnz) = 1./vec(ind_nnz); % normalizing step
    D_P = diag(vec);
    P = P*D_P; % this is a transition tensor
end




