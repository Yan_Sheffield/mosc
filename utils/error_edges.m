function [err_edge] = error_edges(actual,ground_truth,adj_matrix)
num_diff_cuts = size(actual,2); % 5 different cut types for HCut, 1 is ootimal cut and 5 for ECut
err_edge = zeros(num_diff_cuts, 1);

if size(ground_truth,2) == 1
    ground_truth = ground_truth';
end

% labelling nodes for GT clusters
[GT_one,GT_two] = grouped_nodes(ground_truth);
% extract tensors within one cluster
GT_one_edge = adj_matrix(GT_one,GT_one); 
GT_two_edge = adj_matrix(GT_two,GT_two);
total_intra_edges = nnz(GT_one_edge)/2 + nnz(GT_two_edge)/2;


for i = 1:num_diff_cuts
    actual_commu = actual(:,i);
    % we do it in case of different directions of arrays
    if size(actual(:,i),2) == 1
        actual_commu = actual_commu';
    end
    % labelling nodes for actual clusters
    [actual_one,actual_two] = grouped_nodes(actual_commu);
    % matching parallel
    A1_G1 = intersect(actual_one,GT_one);
    A1_G1_edge = nnz(adj_matrix(A1_G1,A1_G1))/2;
    A2_G2 = intersect(actual_two,GT_two);
    A2_G2_edge = nnz(adj_matrix(A2_G2,A2_G2))/2;
    
    % matching cross
    A1_G2 = intersect(actual_one,GT_two);
    A1_G2_edge = nnz(adj_matrix(A1_G2,A1_G2))/2;
    A2_G1 = intersect(actual_two,GT_one);
    A2_G1_edge = nnz(adj_matrix(A2_G1,A2_G1))/2;
    
    total_parallel = A1_G1_edge + A2_G2_edge;
    total_cross = A1_G2_edge + A2_G1_edge;
    % select more matching clusters as right clusters
    if total_parallel > total_cross
        err_edge(i) = total_intra_edges - total_parallel;
        continue 
    end
    
    if total_cross > total_parallel
        err_edge(i) = total_intra_edges - total_cross;
        continue
    end
    
    if total_parallel == total_cross
        % whatever which value is chosen
        err_edge(i) = total_intra_edges - total_parallel;
    end
end
end

function[cluster_one,cluster_two] = grouped_nodes(cluster_indices)
[~,cluster_one,~] = find(cluster_indices==1);
[~,cluster_two,~] = find(cluster_indices==2);
end

