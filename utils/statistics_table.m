statistics_SNAP('DBLP',500)

function statistics_SNAP(dataset,top_num)
% load T_connections
load(['data/commu_property/' upper(dataset) '/max/T_connections_' int2str(top_num) '.mat']);
% load single community property
load(['data/commu_property/' upper(dataset) '/T_single_commu_properties.mat'])

node_range = T_connections{:,2};
min_node = min(node_range);
max_node = max(node_range);
disp('min_node');disp(min_node);
disp('max_node');disp(max_node);

edge_interation_range = T_connections{:,4};
min_edge = min(edge_interation_range);
max_edge = max(edge_interation_range);


pairwise_combins = T_connections{:,{'pair_indx'}};
% split pair
pairwise_combins = split(pairwise_combins,'_');
pairwise_combins = str2double(pairwise_combins);

all_combinations = size(pairwise_combins,1);

triangle_density_array = zeros(all_combinations,1);

for row_combins = 1:all_combinations
    cluster_ind_1 = pairwise_combins(row_combins,1);
    cluster_ind_2 = pairwise_combins(row_combins,2);
    total_density = T_single_commu_properties_dblp{cluster_ind_1,6}+T_single_commu_properties_dblp{cluster_ind_2,6};
    triangle_density_array(row_combins) = total_density;
end
min_dense = min(triangle_density_array);
max_dense = max(triangle_density_array);
disp('min_T');disp(min_dense);
disp('min_T');disp(max_dense);
disp('min_edge');disp(min_edge);
disp('min_edge');disp(max_edge);
% medium position of edge interation
medium_row = size(T_connections,1)/2;
T_connections = sortrows(T_connections,4);
disp('medium edges');disp(T_connections(medium_row,4));
end






% load('data/commu_property/YOUTUBE/T_sorted_triangle_density_youtube.mat')
% % initial edge density  = |E| /|n|, but now, = 2 |E| / |n|
% T_sorted_triangle_density_youtube{:,{'edge_density'}} = T_sorted_triangle_density_youtube{:,{'edge_density'}} * 2;
% save('data/commu_property/DBLP/T_sorted_triangle_density_youtube.mat','T_sorted_triangle_density_youtube');


% dataName = 'LJ';
% top_num = 50;
% cal_size_AD_TPR(dataName,top_num)
% 
% function cal_size_AD_TPR(dataset,top_num)
% path = sprintf('data/commu_property/%s/T_sorted_triangle_density_%s.mat',upper(dataset),dataset);
% load(path);
% 
% if strcmp(dataset, 'dblp')
%     toDelete = T_sorted_triangle_density_dblp.node_num>200;
%     T_sorted_triangle_density_dblp(toDelete,:) = [];
%     node = T_sorted_triangle_density_dblp{1:top_num,'node_num'};
%     edge_dens = T_sorted_triangle_density_dblp{1:top_num,'edge_density'};
%     triangle_dens = T_sorted_triangle_density_dblp{1:top_num,'triangle_density'};
% end
% if strcmp(dataset, 'amazon')
%     toDelete = T_sorted_triangle_density_amazon.node_num>200;
%     T_sorted_triangle_density_amazon(toDelete,:) = [];
%     node = T_sorted_triangle_density_amazon{1:top_num,'node_num'};
%     edge_dens = T_sorted_triangle_density_amzazon{1:top_num,'edge_density'};
%     triangle_dens = T_sorted_triangle_density_amazon{1:top_num,'triangle_density'};
% end
% if strcmp(dataset, 'youtube')
%     toDelete = T_sorted_triangle_density_youtube.node_num>200;
%     T_sorted_triangle_density_youtube(toDelete,:) = [];
%     node = T_sorted_triangle_density_youtube{1:top_num,'node_num'};
%     edge_dens = T_sorted_triangle_density_youtube{1:top_num,'edge_density'};
%     triangle_dens = T_sorted_triangle_density_youtube{1:top_num,'triangle_density'};
% end
% 
% if strcmp(dataset, 'LJ')
%      toDelete = T_sorted_triangle_density_LJ.node_num>200;
%      T_sorted_triangle_density_LJ(toDelete,:) = [];
%      node = T_sorted_triangle_density_LJ{1:top_num,'node_num'};
%      edge_dens = T_sorted_triangle_density_LJ{1:top_num,'edge_density'};
%      triangle_dens = T_sorted_triangle_density_LJ{1:top_num,'triangle_density'};
% end
% 
% if strcmp(dataset, 'orkut')
%     toDelete = T_sorted_triangle_density_orkut.node_num>200;
%     T_sorted_triangle_density_orkut(toDelete,:) = [];
%     node = T_sorted_triangle_density_orkut{1:top_num,'node_num'};
%     edge_dens = T_sorted_triangle_density_orkut{1:top_num,'edge_density'};
%     triangle_dens = T_sorted_triangle_density_orkut{1:top_num,'triangle_density'};
% end
% min_node = min(node);
% max_node = max(node);
% avg_node = sum(node,1)/top_num;
% disp(min_node);disp(max_node);disp(avg_node)
% disp('------')
% min_edge_dens = min(edge_dens);
% max_edge_dens = max(edge_dens);
% avg_edge_dens = sum(edge_dens,1)/top_num;
% disp(min_edge_dens);disp(max_edge_dens);disp(avg_edge_dens);
% disp('------')
% min_tri_dens = min(triangle_dens);
% max_tri_dens = max(triangle_dens);
% avg_tri_dens = sum(triangle_dens)/top_num;
% disp(min_tri_dens);disp(max_tri_dens);disp(avg_tri_dens);
% end




