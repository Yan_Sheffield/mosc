function [actual_labelled_set,t_kmeans] = run_kmeans(eigvec,communities_num)
tic
if isreal(eigvec)
    actual_labelled_set = kmeans(eigvec,communities_num,'emptyaction','singleton');
else
    eigvec = real(eigvec);
    actual_labelled_set = kmeans(eigvec,communities_num,'emptyaction','singleton');
end
t_kmeans = toc;
end

