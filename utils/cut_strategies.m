function [Result_SC,err_SC,err_NMI,err_triangle,err_edge,triangle_density_vec,edge_density_vec,t_sweep] = cut_strategies(G,P_no_seloop,node_num,cut_order,total_motifs,true_labels,isrefined,isrealData,last_two_eigvec,algType,motif_adj,lambda)

if isrealData == 0
    Result_SC = zeros(10,1);
    err_SC = zeros(12,1);
    err_triangle = zeros(12,1);
    err_edge = zeros(12,1);
    err_NMI = zeros(12,1);
    triangle_density_vec = zeros(10,1);%(5-HCut,1-optimalError,5-ECut,1-kmeans)
    edge_density_vec = zeros(10,1);
    t_sweep = zeros(7,1);
    for i = 1:2
        if i == 1
            isHCut = '1';% HCut
        else
            isHCut = '0';% ECut
        end
        [conduc,expension,density,nassoc,ncut,minmaxcut,best_cut_before_refined,best_cut_before_refined_nmi,~,~,actual_labelled_cut_tri,actual_labelled_cut_edge,triangle_array,edge_array,t_sweep_cut] = sweepCut(G,P_no_seloop,node_num,cut_order,total_motifs,true_labels,isrefined,isrealData,isHCut,algType,motif_adj,lambda);
%         [conduc,expension,ncut,minmaxcut] = handle_cut_exceptions(conduc,expension,ncut,minmaxcut);
        Result_SC((i-1)*5+1:i*5)  = [conduc;expension;nassoc;ncut;minmaxcut];
        if i ==1
            t_sweep(1:3) = t_sweep_cut;
            err_SC(1:6) = best_cut_before_refined * node_num;
            err_NMI(1:6) = best_cut_before_refined_nmi;
            err_triangle(1:6) = error_triangles(actual_labelled_cut_tri,true_labels,P_no_seloop,node_num);
            err_edge(1:6) = error_edges(actual_labelled_cut_edge,true_labels,G);
            triangle_density_vec(1:5) = triangle_array;
            edge_density_vec(1:5) = edge_array;
        else
            t_sweep(4:6) = t_sweep_cut;
            err_SC(7:11) = best_cut_before_refined* node_num;
            err_NMI(7:11) = best_cut_before_refined_nmi;
            err_triangle(7:11) = error_triangles(actual_labelled_cut_tri,true_labels,P_no_seloop,node_num);
            err_edge(7:11) = error_edges(actual_labelled_cut_edge,true_labels,G);
            triangle_density_vec(6:10) = triangle_array;
            edge_density_vec(6:10) = edge_array;
        end
    end
    
    [actual_labelled_set,t_kmeans] = run_kmeans(last_two_eigvec,2);
    kmeans_err_nodes = computeError(actual_labelled_set,true_labels)*node_num;
    kmeans_nmi = nmi(actual_labelled_set',true_labels);
    t_sweep(7) = t_kmeans;
    err_SC(12) = kmeans_err_nodes;
    err_NMI(12) = kmeans_nmi;
    err_triangle(12) = error_triangles(actual_labelled_set,true_labels,P_no_seloop,node_num);
    err_edge(12) = error_edges(actual_labelled_set,true_labels,G); 
end
end

