function [W_sum_dan_tensor] = sum_dangling_tensor(tans_tensor,n_node)
W_sum_dan_tensor = zeros(n_node);
for j = 1:n_node
    slice = tans_tensor(:,(j-1)*n_node+1:j*n_node);
    W_sum_dan_tensor = slice + W_sum_dan_tensor;
end
% averaged enties of summed W
W_sum_dan_tensor = W_sum_dan_tensor./n_node;
end