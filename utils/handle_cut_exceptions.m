%% this function is used to handle some unrealistic cut value such that we can plot them properly 
function [conduc,expension,ncut,minmaxcut] = handle_cut_exceptions(conduc,expension,ncut,minmaxcut)
if conduc == 10e6
    conduc = 2;
end
if expension == 10e6
    expension = 2;
end
if ncut == 10e6
    ncut = 4;
end
if minmaxcut == 10e6
    minmaxcut = 4;
end
end

