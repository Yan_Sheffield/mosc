function sort_tables()
load('/Users/geyan/Documents/MATLAB/hypergraph_methods/clustering/data/commu_property/ORKUT/T_single_commu_properties_orkut.mat');
for i = 1:5
    if i == 1
        T_sorted_node_orkut = sortrows(T_single_commu_properties_orkut,2,'descend');
        save('/Users/geyan/Documents/MATLAB/hypergraph_methods/clustering/data/commu_property/ORKUT/T_sorted_node_orkut.mat','T_sorted_node_orkut');
    end
    if i == 2
        T_sorted_edge_orkut = sortrows(T_single_commu_properties_orkut,3,'descend');
        save('/Users/geyan/Documents/MATLAB/hypergraph_methods/clustering/data/commu_property/ORKUT/T_sorted_edge_orkut.mat','T_sorted_edge_orkut');
    end
    if i == 3
        T_sorted_triangle_orkut = sortrows(T_single_commu_properties_orkut,4,'descend');
        save('/Users/geyan/Documents/MATLAB/hypergraph_methods/clustering/data/commu_property/ORKUT/T_sorted_triangle_orkut.mat','T_sorted_triangle_orkut');
    end
    if i == 4
        T_sorted_edge_density_orkut = sortrows(T_single_commu_properties_orkut,5,'descend');
        save('/Users/geyan/Documents/MATLAB/hypergraph_methods/clustering/data/commu_property/ORKUT/T_sorted_edge_density_orkut.mat','T_sorted_edge_density_orkut');
    end
    if i == 5
        T_sorted_triangle_density_orkut = sortrows(T_single_commu_properties_orkut,6,'descend');
        save('/Users/geyan/Documents/MATLAB/hypergraph_methods/clustering/data/commu_property/ORKUT/T_sorted_triangle_density_orkut.mat','T_sorted_triangle_density_orkut');
    end
    
end
end

