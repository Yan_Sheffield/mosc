function [eigvec,last_two_eigvec] = sort_eigen(W,algType,node_num)
[eigvec,eigval] = eig(full(W));
% replace all NaN entries with 0
% eigvec(isnan(eigvec)) = 0;
% calculate second largest eigenvector
if strcmp(algType,'TTM') || strcmp(algType,'TSC') || strcmp(algType,'MinMax')||strcmp(algType,'Hybrid')||strcmp(algType,'RW_SC')||strcmp(algType,'HOSVD')||strcmp(algType,'MOSC_RW_2')
    % sort eigenvector by decending
    temp = sortrows([diag(eigval) eigvec'],'descend');
    if strcmp(algType,'TSC')||strcmp(algType,'Hybrid')||strcmp(algType,'RW_SC')||strcmp(algType,'MOSC_RW_2')
        % last two eigvec is for kmeans
        last_two_eigvec = temp(1:2,2:end)';
        % second largest eigenvector
        eigvec = temp(2,2:end)';
    else
        [eigvec,last_two_eigvec] = norm_step(temp,node_num);
    end
end
 
% calculate second smallest eigenvector
if strcmp(algType,'norm_SC')
    % sort eigenvector by acending
    temp = sortrows([diag(eigval) eigvec'],'ascend');
    [eigvec,last_two_eigvec] = norm_step(temp,node_num);
end

if strcmp(algType,'MOSC2')||strcmp(algType,'MOSC2_norm') 
    % sort eigenvector by acending
    temp = sortrows([diag(eigval) eigvec'],'ascend');
    % second smallest eigenvector for sweep cut.
    eigvec = temp(2,2:end)';
    % eigenvectors for k-means
    [~,last_two_eigvec] = norm_step(temp,node_num);
end
end


 
function[eigvec,last_two_eigvec] =  norm_step(temp,node_num)
eigvec = temp(1:2,2:end)';
for i = 1:node_num
    if (norm(eigvec(i,:))>0)
        eigvec(i,:) = eigvec(i,:)./norm(eigvec(i,:));
    end
end
last_two_eigvec = eigvec;
% second smallest eigenvector
eigvec = eigvec(:,2);
end

