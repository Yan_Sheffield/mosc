function error = multi_partition_error_edges(actual,ground_truth,G)
cluster_No = max(actual);
max_comparison  = cluster_No*10;
% turn actual labels (1 1 2 2) into index (1 2;3 4)
actual_index_matrix = grouped_nodes(actual,cluster_No);

% step I: randomly match between ground-truth and actual

% q is the order for actual clusters
q_A = randperm(cluster_No,cluster_No)';
% p is the order for ground-truth
p_G = (1:cluster_No)';
% the fisrt column is ground-truth and the second column is actual
% the third column is for fitness (error edges)
pairs = [p_G q_A zeros(cluster_No,1)];
% step II: compute first generation error edge
% max_actual = 0;
for m = 1:size(q_A,1)
    % select actual cluster by q
    one_actual_cluster =  actual_index_matrix{q_A(m)};
    % select ground-truth cluster by given order
    one_ground_truth_cluster = ground_truth(p_G(m),:);
    intersection = intersect(one_actual_cluster,one_ground_truth_cluster);
    if isempty(intersection)
        pairs(m,3) = 0;
    else
        pairs(m,3) = nnz(G(intersection,intersection))/2;
    end
    %     max_actual = max_actual+pairs(m,3);
end
fitness = sum(pairs(:,3));
% step III: randomly swap position between actual clusters

for i = 1:max_comparison
    % randomly select two different positions to swap
    mutation_row = randperm(cluster_No,2);
    first_actual_cluster = actual_index_matrix{pairs(mutation_row(1),2)};
    second_actual_cluster = actual_index_matrix{pairs(mutation_row(2),2)};
    
    first_ground_truth_cluster = ground_truth(pairs(mutation_row(1),1),:);
    second_ground_truth_cluster = ground_truth(pairs(mutation_row(2),1),:);
    
    fitness_before_swap = pairs(mutation_row(1),3) + pairs(mutation_row(2),3);
    
    % swap the pair between actual clusters
    %     pairs([cluster_No+mutation_row(1) cluster_No+mutation_row(2)]) = pairs([cluster_No+mutation_row(2) cluster_No+mutation_row(1)]);
    intersection_one = intersect(first_actual_cluster,second_ground_truth_cluster);
    intersection_two = intersect(second_actual_cluster,first_ground_truth_cluster);
    
    fitness_one = nnz(G(intersection_one,intersection_one))/2;
    fitness_two = nnz(G(intersection_two,intersection_two))/2;
    new_fitness = fitness_one + fitness_two;
    
    % step IV: filtering process: buffer range is 20
    
    % step V: satisfy buffer: calculate error edge for new swaps and update
    % fitness
    
    % step VI: selection: keep offspring if better otherwise keep parents
    if new_fitness > fitness_before_swap
        fitness = fitness + (new_fitness - fitness_before_swap);
        % update swap
        pairs([cluster_No+mutation_row(1) cluster_No+mutation_row(2)]) = pairs([cluster_No+mutation_row(2) cluster_No+mutation_row(1)]);
        % update fitness array
        pairs(mutation_row(2),3) = fitness_one;
        pairs(mutation_row(1),3) = fitness_two;
    end
end
% todo: not total but insider triangle
error = total_edge_or_triangle - fitness;

end

function[actual_index_matrix] = grouped_nodes(cluster_label,cluster_No)
actual_index_matrix = cell(cluster_No,1);
for i = 1:cluster_No
    [~,actual_index_matrix{i},~] = find(cluster_label==i);
end
end

