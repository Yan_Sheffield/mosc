function addpaths()
addpath(genpath('algorithms/'))
addpath(genpath('utils/'))
addpath(genpath('criteria/'))
addpath(genpath('data/'))
addpath(genpath('plots/'))
addpath(genpath('utils/tensor_pageRank/'))
addpath(genpath('utils/HOSPLOC/'))
addpath(genpath('explore_algs/'));
end

