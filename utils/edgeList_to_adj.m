function edgeList_to_adj(edgeListTextFile,direction)
%% convert an edgeList to a adjacency matrix and save it as .mat file
edgeList = readtable(edgeListTextFile);
% x_FromNodeId and ToNodeId is the variable of coloumn in
% a table.
fromNodeId = edgeList.x_FromNodeId+1;
toNodeId = edgeList.ToNodeId+1; % edgeLists actuall start with 0.
% construct graph from row matrix, directed graph
if direction == 0
    % directed graphs
    G = digraph(fromNodeId',toNodeId');
else
    % undirected graphs
    G = graph(fromNodeId',toNodeId');
end
% p = plot(G);
% p.NodeColor = 'red';
%adjacency matrix.
% 1.this matrix does not include all nodes since not all nodes have edge.
com_LJ = adjacency(G);
save('com_LJ.mat','com_LJ');
end

% handle polbook dataset

% function edgeList_to_adj(edgeListTextFile,direction)
% %% convert an edgeList to a adjacency matrix and save it as .mat file
% edgeList = readtable(edgeListTextFile);
% % x_FromNodeId and ToNodeId is the variable of coloumn in
% % a table.
% fromNodeId = edgeList.Var1;
% toNodeId = edgeList.Var2; % edgeLists actuall start with 0.
% % construct graph from row matrix, directed graph
% if direction == 0
%     % directed graphs
%     G = digraph(fromNodeId',toNodeId');
% else
%     % undirected graphs
%     G = graph(fromNodeId',toNodeId');
% end
% % p = plot(G);
% % p.NodeColor = 'red';
% %adjacency matrix.
% % 1.this matrix does not include all nodes since not all nodes have edge.
% adj_matrix = adjacency(G);
% save('polbooks.mat','adj_matrix');
% end



