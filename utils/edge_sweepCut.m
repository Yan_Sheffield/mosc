function [conduc,expension,density,nassoc,ncut,minmaxcut,err_conduc,err_expen,err_dens,err_nassoc,err_ncut,err_min,best_cut_before_refined] = edge_sweepCut(G,P_no_seloop,n_node,cut_order,total_motifs,true_labels,isrefined,isrealData)
best_cluster_conduc = NaN; % TODO?1. combine return value of sweep cut 2. indicator vector for ground truth
best_cut_conduc = NaN;
best_assoc_S_conduc = NaN;
best_assoc_S_bar_conduc = NaN;

best_cluster_expen = NaN;
best_cut_expen = NaN;
best_assoc_S_expen = NaN;
best_assoc_S_bar_expen = NaN;

best_cluster_ncut = NaN;
best_cut_ncut = NaN;
best_assoc_S_ncut = NaN;
best_assoc_S_bar_ncut = NaN;

best_cluster_min = NaN;
best_cut_min = NaN;
best_assoc_S_min = NaN;
best_assoc_S_bar_min = NaN;

best_cluster_dens = NaN;
best_cut_dens = NaN;
best_assoc_S_dens = NaN;
best_assoc_S_bar_dens = NaN;

best_cluster_nassoc = NaN;
best_cut_nassoc = NaN;
best_assoc_S_nassoc = NaN;
best_assoc_S_bar_nassoc = NaN;

err_conduc = NaN;
err_expen = NaN;
err_ncut = NaN;
err_min = NaN;
err_dens = NaN;
err_nassoc = NaN;

best_cut_before_refined = NaN;
for j = 1:n_node-1 % Sweep cuts TODO:need to further verify
    C = cut_order(1:j)';
    C_bar = cut_order(j+1:n_node)';
    [conduc,expension,density,nassoc,ncut,minmaxcut,assoc_S,assoc_S_bar]  = ECut(G,P_no_seloop,C,C_bar,n_node,total_motifs,'Triangle','undirected');

    [best_cluster_expen,best_cut_expen,best_assoc_S_expen,best_assoc_S_bar_expen] = filter_Cluster(expension,C,best_cluster_expen,best_cut_expen,'expen',j,assoc_S,assoc_S_bar,best_assoc_S_expen,best_assoc_S_bar_expen);
    
    [best_cluster_conduc,best_cut_conduc,best_assoc_S_conduc,best_assoc_S_bar_conduc] = filter_Cluster(conduc,C,best_cluster_conduc,best_cut_conduc,'conduc',j,assoc_S,assoc_S_bar,best_assoc_S_conduc,best_assoc_S_bar_conduc);
    
    [best_cluster_ncut,best_cut_ncut,best_assoc_S_ncut,best_assoc_S_bar_ncut] = filter_Cluster(ncut,C,best_cluster_ncut,best_cut_ncut,'ncut',j,assoc_S,assoc_S_bar,best_assoc_S_ncut,best_assoc_S_bar_ncut);
    
    [best_cluster_min,best_cut_min,best_assoc_S_min,best_assoc_S_bar_min] = filter_Cluster(minmaxcut,C,best_cluster_min,best_cut_min,'min',j,assoc_S,assoc_S_bar,best_assoc_S_min,best_assoc_S_bar_min);
    
    [best_cluster_dens,best_cut_dens,best_assoc_S_dens,best_assoc_S_bar_dens] = filter_Cluster(density,C,best_cluster_dens,best_cut_dens,'dens',j,assoc_S,assoc_S_bar,best_assoc_S_dens,best_assoc_S_bar_dens);
    
    [best_cluster_nassoc,best_cut_nassoc,best_assoc_S_nassoc,best_assoc_S_bar_nassoc] = filter_Cluster(nassoc,C,best_cluster_nassoc,best_cut_nassoc,'nassoc',j,assoc_S,assoc_S_bar,best_assoc_S_nassoc,best_assoc_S_bar_nassoc);
end

best_labelled_conduc = label_nodes(best_cluster_conduc,n_node);
best_labelled_expen = label_nodes(best_cluster_expen,n_node);
best_labelled_ncut = label_nodes(best_cluster_ncut,n_node);
best_labelled_min = label_nodes(best_cluster_min,n_node);
best_labelled_dens = label_nodes(best_cluster_dens,n_node);
best_labelled_nassoc = label_nodes(best_cluster_nassoc,n_node);

% we don't calculate error rate for each cut if data is from real world.
% we only calculate error after finishing all cuts.
if isrealData == 0
    err_conduc = computeError(best_labelled_conduc,true_label);
    err_expen = computeError(best_labelled_expen,true_label);
    err_ncut = computeError(best_labelled_ncut,true_label);
    err_min = computeError(best_labelled_min,true_label);
    err_dens = computeError(best_labelled_dens,true_label);
    err_nassoc = computeError(best_labelled_nassoc,true_label);
    % these cuts are not affected by refinement method
    best_cut_before_refined = [err_conduc;err_expen;err_dens;err_nassoc;err_ncut;err_min];
end
end

function [best_cluster,best_cut,best_assoc_S,best_assoc_S_bar] = filter_Cluster(HCut,C,best_cluster,best_cut,cut_Type,j,assoc_S,assoc_S_bar,best_assoc_S,best_assoc_S_bar )

if j == 1
    best_cut = HCut;
    best_cluster = C;
    best_assoc_S = assoc_S;
    best_assoc_S_bar = assoc_S_bar;
else  % it suits for criteria that smaller the better
    if (best_cut >= HCut) && (strcmp(cut_Type, 'conduc')||strcmp(cut_Type, 'expen')||strcmp(cut_Type, 'ncut')||strcmp(cut_Type, 'min'))
        best_cut = HCut;
        best_cluster = C;
        best_assoc_S = assoc_S;
        best_assoc_S_bar = assoc_S_bar;
    end
    if (best_cut <= HCut) && (strcmp(cut_Type, 'dens')||strcmp(cut_Type, 'nassoc'))
        best_cut = HCut;
        best_cluster = C;
        best_assoc_S = assoc_S;
        best_assoc_S_bar = assoc_S_bar;
    end
end

end

