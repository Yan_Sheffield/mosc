function [unfolding_adj_tensor] = construct_adj_tensor(G)

% n is the number of nodes
n = size(G,1);
% one Q: why speye? It's wrong. we can count the right number of triangles
% after removing it
unfolding_adj_tensor = construct_triangle(G,n);% for counting motifs
% G = G + speye(n);
% P = construct_triangle(G,n);% for HOSLOC and TSC
end

function[P] = construct_triangle(G,n)
    P = sparse(n,n*n);
    for k = 1:n
        % firstK indicates that the connection situation of node_1->other node
        firstK = repmat(G(k,:),n,1);
        % secondK indicates that other node of connection situation with node_1
        secondK = repmat(G(:,k),1,n);
        % firstK is the first edge
        % firstK .*G indicates the second edge of connection situation
        % .* secondK indicates whether the third edge can come back to first
        % node.
        temp = (firstK.*G);
        P(:,(k-1)*n+1:k*n) = temp.*secondK;
    end
end

