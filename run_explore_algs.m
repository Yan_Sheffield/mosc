function run_explore_algs(conType,dataset,top_num,run_TSC,run_HOSLOC,bi_or_multiple)
% start to explore lambda value
addpath('utils/')
addpaths();

% load ground truth
load(['data/real_world/ground_truth_matrix/com_' dataset '.mat']);
% load adj matrix
load(['data/real_world/adj_matrix/full_adj_matrix/com_' dataset '.mat'])


adj = double(adj_matrix);
GT_matrix = ground_truth+1;

% this graph is undirected,so this matrix should be symmetrical
if ~issymmetric(adj)
    adj = adj+ adj.';
    adj(adj>1) = 1;
end

label_pairwise_ground_truth(adj,GT_matrix,dataset,top_num,conType,run_TSC,run_HOSLOC,bi_or_multiple);

end


function label_pairwise_ground_truth(adj,GT_matrix,dataset,top_num,conType,run_TSC,run_HOSLOC,bi_or_multiple)

citeria_num = 10;
algs_num = 2;
score_algs = zeros(citeria_num,algs_num);

sweep_types = 12; % % HCut(5) +optimal error(1)+ ECut(5) + kmeans

error_node_algs = zeros(sweep_types,algs_num);

error_triangles_algs = zeros(sweep_types,algs_num);

error_edge_algs = zeros(sweep_types,algs_num);

NMI = zeros(sweep_types,algs_num);

isrealData = 0;

result_err_node = zeros(12,algs_num);
result_NMI = zeros(12,algs_num);
result_score = zeros(10,algs_num);
result_err_triangle = zeros(12,algs_num);
result_err_edge = zeros(12,algs_num);


time_matrix = zeros(12,algs_num);
time_tensor = zeros(12,algs_num);

num_clusters = 5;

num_cut = 10;
triangle_density_matrix = zeros(num_cut,algs_num);
triangle_density_tensor = zeros(num_cut,algs_num);

edge_density_matrix = zeros(num_cut,algs_num);
edge_desnity_tensor = zeros(num_cut,algs_num);

% save results to the specific folder when only running TSC
% if strcmp(run_TSC,'on')
%     is_TSC_HOSLOC = 'run_TSC';
% end
% if strcmp(run_HOSLOC,'on')
%     is_TSC_HOSLOC = 'run_HOSLOC';
% end
% this is for bi-partition for Meacheal's datasets
if strcmp(dataset, 'adjnoun') || strcmp(dataset, 'polblogs')||strcmp(dataset,'zachary')||strcmp(dataset,'dolphin')
    row_combins = 1;
    cut_order_total = cell(algs_num,1);
    true_label_index = NaN;
else
    % the below codes are for multiple partition
    total_num_nodes = size(adj,2);
    row_combins = 0;
    cut_order_total = 1;
    isrealData = 0;
    % Mark's datasets have vector ture labels but do not have label
    % index with cell matrix way
    num_clusters = max(GT_matrix);
    [true_labels, true_label_index] = trans_label_to_label_matrix(GT_matrix,num_clusters);
end
call_algorithms(adj,GT_matrix,size(adj,1),num_clusters,score_algs,error_node_algs,NMI,error_triangles_algs,error_edge_algs,isrealData,dataset,top_num,row_combins,cut_order_total,algs_num,triangle_density_matrix,edge_density_matrix,time_matrix,run_TSC,run_HOSLOC,bi_or_multiple,true_label_index);

end


function [score_algs,error_node_algs,NMI,error_triangles_algs,error_edges_algs,cut_order_total,triangle_density_matrix,edge_density_matrix,time_matrix] = call_algorithms(plant_model,true_labels,total_num_nodes,num_clusters,score_algs,error_node_algs,NMI,error_triangles_algs,error_edges_algs,isrealData,dataset,top_num,row_combins,cut_order_total,algs_num,triangle_density_matrix,edge_density_matrix,time_matrix,run_TSC,run_HOSLOC,bi_or_multiple,true_label_index)


% unfolding_adj_tensor is used to calculate cuts value
[unfolding_adj_tensor,tans_tensor,vec,t_adj_tensor,t_trans_tensor] = transTensor(plant_model); % P_no_seloop is for counting cut/vol/assoc
% adj_tensor_total{1,row_combins} = unfolding_adj_tensor;

total_motifs = nnz(unfolding_adj_tensor)/2;  % every motif is counted for twice for each node


% this operator is for TTM]
% adj_tensor is used to calculate error triangles
% [sum_tensor] = sum_unfolding_tensor(unfolding_adj_tensor,total_num_nodes);

% [W_sum_dan_tensor] = sum_dangling_tensor(trans_tensor_dangling,total_num_nodes);

% they are intra-edges and triangles for multiple partitions of
% Mecheal's
if strcmp(bi_or_multiple,'multiple')
    if strcmp(dataset, 'polbooks') || strcmp(dataset, 'football')
        [intra_triangles,intra_edges] = count_intra_edge_triangle(true_label_index,num_clusters,plant_model,unfolding_adj_tensor,total_num_nodes);
    else
        % SNAP's dataset
        [intra_triangles,intra_edges] = intra_edge_triangle_property_table(dataset);
    end
else
    intra_triangles = 0;
    intra_edges = 0;
end


tic
[RW_matrix] = construct_RW_matrix(plant_model);
t_RW_Lap = toc;

tic
motif_adj = MotifAdjacency(plant_model, 'M4');
t_adj_motif = toc;

iterated_cluster_num = NaN;
targeted_cluster = NaN;
num_communities = NaN;
seperated_label = NaN;

actual_labels_TSC = NaN;
actual_labels_Science = NaN;
actual_labels_rw_MinMax = NaN;
actual_labels_refined_rw_MinMax = NaN;


lambda = 0.5;
[err_node,NMI,err_triangle,err_edge,~,t_Lap_eigen_cut] = MOSC_RW(RW_matrix,plant_model,plant_model,unfolding_adj_tensor,total_motifs,true_labels,total_num_nodes,isrealData,iterated_cluster_num,targeted_cluster,actual_labels_rw_MinMax,num_communities,seperated_label,'0',lambda,dataset,top_num,row_combins,cut_order_total,tans_tensor,motif_adj,bi_or_multiple,intra_edges,intra_triangles,true_label_index,num_clusters);
disp('---MOSC-RW:----');
disp('---NMI----');disp(NMI);
disp('---error nodes----');disp(err_node);
disp('---error edge----');disp(err_edge);
disp('---error triangles----');disp(err_triangle);



[err_node,NMI,err_triangle,err_edge,~,t_Lap_eigen_cut] = MOSC_GL(plant_model,plant_model,unfolding_adj_tensor,total_motifs,true_labels,total_num_nodes,isrealData,iterated_cluster_num,targeted_cluster,actual_labels_rw_MinMax,num_communities,seperated_label,'0',lambda,dataset,top_num,row_combins,cut_order_total,motif_adj,bi_or_multiple,intra_edges,intra_triangles,true_label_index,num_clusters);

disp('---MOSC-GL:----');
disp('---NMI----');disp(NMI);
disp('---error nodes----');disp(err_node);
disp('---error edge----');disp(err_edge);
disp('---error triangles----');disp(err_triangle);


end


function all_pair_index = top_triangle_density(dataset,top_num,conType)
% path = sprintf('data/commu_property/%s/T_sorted_triangle_density_%s.mat',upper(dataset),dataset);
load(['data/commu_property/' upper(dataset) '/' conType '/T_connections_' int2str(top_num) '.mat']);
% all rows
all_pair_index = T_connections{:,{'pair_indx'}};
% split pair
all_pair_index = split(all_pair_index,'_');
all_pair_index = str2double(all_pair_index);
end


function[L] = construct_RW_matrix(G)
degs = sum(G, 2);
% D    = sparse(1:size(G, 1), 1:size(G, 2), degs);% construct a degree matrix

% compute unnormalized Laplacian
% TODO: remove D in order to calculate second largest eigenvector that
% is consistent with 3D tensor

% add a value to diagnal
degs(degs == 0) = eps;
% produce a new matrix whose diagnoal is 1./degs
D = spdiags(1./degs, 0, size(G,1), size(G,1));

% calculate normalized Laplacian
L = D*G;

end


function W = MotifAdjacency(A, motif)
% Ignore diagonals and weights
A = A - diag(diag(A));
A = min(A, 1);% maximum entities of A is 1
lmotif = lower(motif);
if strcmp(lmotif, 'm4')
    W = M4(A);
end
end

function W = M4(A)
[B, ~, ~] = DirectionalBreakup(A);
W = (B * B) .* B;
end

function [B, U, G] = DirectionalBreakup(A)
% DIRECTIONALBREAKUP returns the bidirectional, unidirectional, and
% undirected versions of the adjacency matrix A.
%
% [B, U, G] = DirectionalBreakup(A) returns
%   B: the bidirectional subgraph
%   U: the unidirectional subgraph
%   G: the undirected graph
%
%  Note that G = B + U

A(find(A)) = 1;
B = spones(A&A');  % bidirectional
U = A - B; % unidirectional
G = A | A';
end


function [true_label_form,true_label_index] = handle_true_labels(true_labels,total_num_nodes,num_clusters)
true_label_form = zeros(total_num_nodes,1);
true_label_index = cell(num_clusters,1);
start_point = 1;
% I. remove zeros from true_labels
% II. transform ture_labels to [1 1 1 2 2 2 3 3 3] format
for i = 1:size(true_labels,1)
    % find non-zero elements
    non_zeros_elements_index = find(true_labels(i,:));
    true_label_index{i} = true_labels(i,non_zeros_elements_index);
    total_non_zeros = size(non_zeros_elements_index,2);
    % dynamically change index
    true_label_form(start_point:start_point+total_non_zeros-1) = i;
    start_point = start_point+total_non_zeros;
end
end

% this is for multipartitions
function [num_triangles,num_edges] = count_intra_edge_triangle(cluster_label,cluster_No,G,P_no_seloop,node_num)
num_triangles = 0;
num_edges = 0;
for i = 1:cluster_No
    C = cluster_label{i};
    num_triangle = count_triangles(C,P_no_seloop,node_num);
    num_triangles = num_triangles + num_triangle;
    num_egde = nnz(G(C,C))/2;
    num_edges = num_edges + num_egde;
end
end

function[num_triangles] = count_triangles(C,P_no_seloop,node_num)
% cluster size
n_c = size(C,2);
temp = zeros(1, n_c*n_c);
% calculate all about columns about nodes in one cluster eg.P(C,C)
for i = 1: n_c
    temp((i-1)*n_c+1:i*n_c) = (C(i)-1)*node_num + C;
end
num_triangles = nnz(P_no_seloop(C, temp))/6;
end

% this is for Micheal's datasets. Trans truth label vector to cell label
% array
function [true_label_form,true_label_index] =  trans_label_to_label_matrix(GT_matrix,num_clusters)
true_label_index = cell(num_clusters,1);
true_label_form = GT_matrix;
for i = 1:num_clusters
    true_label_index{i} = find(GT_matrix == i);
end
end

% calculate intra number of edge and triangle from property table
function [intra_triangles,intra_edges] = intra_edge_triangle_property_table(dataset)
load(['data/commu_property/' upper(dataset) '/T_single_commu_properties.mat'])
if strcmp(dataset, 'dblp')
    property_table = T_single_commu_properties_dblp;
end

if strcmp(dataset, 'youtube')
    property_table = T_single_commu_properties_youtube;
end

if strcmp(dataset, 'LJ')
    property_table = T_single_commu_properties_LJ;
end

if strcmp(dataset,'orkut')
    property_table = T_single_commu_properties_orkut;
end
intra_triangles = sum(property_table.triangle_num);
intra_edges = sum(property_table.edge_num);
end