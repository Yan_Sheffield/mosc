function [conduc,expension,density,nassoc,ncut,conduc_mix,optimal_err,optimal_nmi,optimal_err_triangle, optimal_err_edge,assoc_S,assoc_S_bar,t_conduct_3,t_nassoc_3,t_conduc_mix]  = HCut(G, P_no_seloop, C,C_bar, node_num,total_motifs,motif,direction,true_label,motif_adj,lambda)
% Input:
% - G: the adjacency matrix of the given network.
% - P: the constructed transition tensor. 
% - C: the returned structure-rich graph cut. 
% - motif: the user-defined structures.
% Output:
% - Phi_S: the high-order conductance of the returned cluster regarding the user-defined structures. 


if strcmp(motif, 'ThreeNodeLine') || strcmp(motif, 'Triangle')
    tic
    % cluster size 
    n_c = size(C,2); 
    temp = zeros(1, n_c*n_c);
    % calculate all about columns about nodes in one cluster eg.P(C,C)
    for i = 1: n_c
        temp((i-1)*n_c+1:i*n_c) = (C(i)-1)*node_num + C; 
    end
    % eg (1,2,3) and (1,3,2) are the same motif
    vol_S = nnz(P_no_seloop(C,:))/2;
    vol_S_bar = total_motifs - vol_S;
     
    if strcmp(direction, 'directed')
        cut  = vol_S - nnz(P_no_seloop(C, temp));
    end
    
    if strcmp(direction, 'undirected')
        n_c_bar = node_num - n_c;
        temp_bar = zeros(1,n_c_bar*n_c_bar);
        for i = 1: n_c_bar
            temp_bar((i-1)*n_c_bar+1:i*n_c_bar) = (C_bar(i)-1)*node_num + C_bar;
        end
        
        % the the number of nodes connecting all nodes within one cluster
        % (1,2,3) and (1,3,2) are the same motif
        assoc_S = nnz(P_no_seloop(C, temp))/2;
        assoc_S_bar = nnz(P_no_seloop(C_bar,temp_bar))/2;
        % the number of undirected triangles between two clusters
        % a motif as one triangle is counted for three times
        cut = (total_motifs - assoc_S - assoc_S_bar)/3;
    
    end
    public = toc;
    % try to use motif adjacency matrix to calcualte cuts value, but
    % fail to do it. This is beacause this matrix did not consider all
    % three nodes of a motif (only two nodes), which leads to deviation of assoc.
    
%     d_motif = sum(motif_adj,2);
%     % mu_v is the total number of degrees
%     % the actual number of motif is the half of degree
%     total_motif_degree = sum(d_motif)/2;
%     % namta is vol(c_cluster),assoc+cut
%     vol_S = sum(d_motif(C))/2;
%     vol_S_bar = total_motif_degree - vol_S;
%     % is C one intra-edge for one cluster?If so, temp is total number of
%     % edegs within one cluster,associativity
%     assoc_S = sum(sum(motif_adj(C,C)))/2;
%     cut = vol_S - assoc_S;
%     assoc_S_bar = sum(sum(motif_adj(C_bar,C_bar)))/2;
%     n_c = size(C,2);
%     n_c_bar = node_num - n_c;
    
    tic
    conduc = HCondu(cut,vol_S, vol_S_bar);
    t_conduct_3 = toc;
    t_conduct_3 = t_conduct_3 + public;
    
    tic
    nassoc = HNassc(assoc_S,assoc_S_bar,vol_S,vol_S_bar);
    t_nassoc_3 = toc;
    t_nassoc_3 = t_nassoc_3 + public;
    
    % mixed-order cut and volumn;
    % I put here just in case counting time
    tic
    d_edge = sum(G,2);
    total_edges_degree = sum(d_edge);
    assoc_S_edge = sum(sum(G(C,C)));
    vol_S_edge = full(sum(d_edge(C)));
    vol_S_bar_edge = total_edges_degree - vol_S_edge;
    cut_edge = vol_S_edge - assoc_S_edge;
    cut_mix = (1-lambda)*cut + lambda*cut_edge;
    vol_mix = (1-lambda)*vol_S + 2*lambda*vol_S_edge;
    vol_bar_mix = (1-lambda)*vol_S_bar + 2*lambda*vol_S_bar_edge;
    conduc_mix = HMinmaxcut(cut_mix,vol_mix,vol_bar_mix);
    t_conduc_mix = toc;
    t_conduc_mix = t_conduc_mix + public;
    
    expension =  HExpension(cut,n_c,n_c_bar);
    density =  HDensity(vol_S, vol_S_bar,n_c,n_c_bar);
    ncut = HNcut(cut,vol_S, vol_S_bar);
    
    optimal_err = calcu_err_node(C,node_num,true_label); % add edge and triangle
    optimal_nmi = calcu_err_nmi(C,node_num,true_label);
    optimal_err_triangle = calcu_err_triangle(C,node_num,true_label,P_no_seloop);
    optimal_err_edge = calcu_err_edge(C,node_num,true_label,G);
end
end


function conduc = HCondu(cut,vol_S, vol_S_bar)
    min_vol = min(vol_S,vol_S_bar);
    if min_vol == 0
        conduc = 10e6; % the graph cut returned an empty set or the whole graph. 
    else
        conduc = cut/min_vol;
    end
end

function expension =  HExpension(cut,node_c,n_c_bar)
    min_size = min(node_c,n_c_bar);
    if min_size == 0 % publish is not enough
        expension = 10e6; % it may be influence relability and accuracy
    else
        expension = cut/min_size;
    end
end

function density =  HDensity(vol_S, vol_S_bar,node_c,n_c_bar)% TODO: the this defination of density is not heuristical
    if node_c == 0 || n_c_bar == 0
        density = 0; 
    else
        density = max(vol_S/(node_c^2),vol_S_bar/(n_c_bar^2));
    end 
end

function nassoc = HNassc(assoc_S,assoc_S_bar,vol_S,vol_S_bar)
    if vol_S == 0 || vol_S_bar == 0
        nassoc = 0;
    else
        nassoc = assoc_S/vol_S + assoc_S_bar/vol_S_bar;
    end
end

function ncut = HNcut(cut,vol_S, vol_S_bar)
    if vol_S == 0 || vol_S_bar == 0
        ncut = 10e6;
    else
        ncut = cut*(1/vol_S+1/vol_S_bar);
    end
end
% optimal minmaxcut value is larger than 4. so it only reserves clusters
% that value is 4. That is a deviation.
function conduc_mix = HMinmaxcut(cut_mix,vol_mix,vol_bar_mix)
    min_vol = min(vol_mix,vol_bar_mix);
    if vol_mix == 0 || vol_bar_mix == 0
        conduc_mix = 10e6;
    else
        conduc_mix = cut_mix/min_vol;
    end
end

function optimal_err = calcu_err_node(C,node_num,true_label)
optimal_label = label_nodes(C,node_num);
optimal_err = computeError(optimal_label,true_label);
end

function optimal_err = calcu_err_nmi(C,node_num,true_label)
optimal_label = label_nodes(C,node_num);
optimal_err = nmi(optimal_label,true_label);
end

function optimal_err_triangle = calcu_err_triangle(C,node_num,true_label,P_no_seloop)
optimal_label = label_nodes(C,node_num);
optimal_err_triangle = error_triangles(optimal_label,true_label,P_no_seloop,node_num);
end

function optimal_err_edge = calcu_err_edge(C,node_num,true_label,adj_matrix)
optimal_label = label_nodes(C,node_num);
optimal_err_edge = error_edges(optimal_label,true_label,adj_matrix);
end