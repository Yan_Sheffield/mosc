# Mixed-Order Spectral Clustering for Networks

This is experimental Matlab code for the methods: MOSC-GL snd MOSC-RW.

## Example of Zachary Dataset
run `main.m` with Matlab. You will see printed results in Matlab terminal w.r.t NMI, error nodes/edge/triangle, which
are derived from a recommended λ = 0.5.
